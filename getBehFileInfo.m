% get mouse, protocol and date from behavior file name

function p = getBehFileInfo(behFile)

tmp = strfind(behFile,'_');
p.mouse = behFile(1:tmp(1)-1);
p.protocol = behFile(tmp(1)+1:tmp(2)-1);
monthStart = length(p.mouse)+length(p.protocol)+3;
[~,date] = convertBpodDate(behFile,monthStart);
p.date = date{1};
p.behFile = behFile;