% finds matching bonsai .csv and .avi file given a behavior file input

function [bestMatch,bon] = findBonsaiFile(behFile,date,trialDur_beh,bonsaiPath)

% --- find corresponding bonsai files ---
% format of bonsai file: 
% .avi: bon_2019-04-15T16_54_50
% .csv: ts2019-04-15T16_54_50
% 	contains 3 columns: 1) frame counter 2) timestamp 3) GPIO pin state

% --- use .csv file to match recording length ---
dateDashed = ['20' date(1:2) '-' ...
                   date(3:4) '-' ...
                   date(5:6)];
csvFiles = dir([bonsaiPath filesep '*' dateDashed '*.csv']);
aviFiles = dir([bonsaiPath filesep '*' dateDashed '*.avi']);

if ~isempty(csvFiles)
    nFiles = length(csvFiles);
    frameRate_inferred=[]; nTrials_bon=[];
    trialDur_bon_inferred={}; nTrialDiff=[];
    frameTimeStamps={}; frameCounter={};
    
    % loop over .csv files and extract relevant parameters
    % the match is made simply by number of trials behavior vs. bonsai
    % crosscorrelation of trial duration is shown for inspection
    for cc = 1:nFiles
        fileID = fopen(fullfile(bonsaiPath,csvFiles(cc).name));
        tmp = textscan(fileID,'%f64 %f64 %f64');
        fclose(fileID);
        c = cell2mat(tmp);
        frameTimeStamps{cc} = pointGrey_ts_decoder(c(:,2)); % point grey time stamps have a complicated format
        frameCounter{cc} = c(:,1);
        
        % get TTL length to estimate frame rate
        % to get frameRate, assume behavior trial start TTL is up for 3sec
        % first convert ttl to logical (read as uint in bonsai and has large values for reasons I don't understand)
        ttl_logical = (c(:,3)-min(c(:,3))) > 0;
        trialStart_ttl = diff(ttl_logical)'; 
        ttl_up{cc} = find([0 trialStart_ttl == 1]);
        ttl_down = find([0 trialStart_ttl == -1]);
        if ~isempty(ttl_up{cc}) && ~isempty(ttl_down)
            frameRate_inferred(cc) = median(ttl_down - ttl_up{cc})/3;
            nTrials_bon(cc) = length(ttl_up{cc});
            trialDur_bon_inferred{cc} = diff(ttl_up{cc})/frameRate_inferred(cc);
            nTrialDiff(cc) = length(trialDur_bon_inferred{cc})-length(trialDur_beh);
        else
            nTrialDiff(cc) = nan;
        end
    end
    if sum(isnan(nTrialDiff))==length(nTrialDiff)
        error('no GPIO TTLs present in any .csv files on this date')
    end
else
    error('no .csv files found')
end

% --- assign variables to be saved ---
[~,bestMatch.ind] = min(abs(nTrialDiff));
bestMatch.nTrialDiff = nTrialDiff(bestMatch.ind);
bestMatch.csv = csvFiles(bestMatch.ind).name;
bestMatch.csvPath = fullfile(bonsaiPath,bestMatch.csv);
bestMatch.trialDur_bon_inferred = trialDur_bon_inferred{bestMatch.ind};
bestMatch.trialDur_beh = trialDur_beh;
bon.trialStart_samp = ttl_up{bestMatch.ind};
bon.timeStamps = frameTimeStamps{bestMatch.ind};
bon.trialStart_ts = bon.timeStamps(bon.trialStart_samp);
bon.counter = frameCounter{bestMatch.ind}; 
bon.rate_median = 1/median(diff(bon.timeStamps));
bon.nTrials = length(bon.trialStart_samp);


% --- find matching .avi file ---
file_ts = bestMatch.csv(end-11:end-4);
ts_sec = file_ts(7:8);
ts_min = file_ts(4:5);

% account for file names of .avi and .csv straddling change in second or minute
valid_ts{1} = file_ts;
if strcmp(ts_sec,'59')
    valid_ts{2} = [file_ts(1:3) num2str(str2double(ts_min)+1) '00'];
    valid_ts{3} = [file_ts(1:6) num2str(str2double(ts_sec)-1)];
elseif strcmp(ts_sec,'00')
    valid_ts{2} = [file_ts(1:3) num2str(str2double(ts_min)-1) '59'];
    valid_ts{3} = [file_ts(1:6) num2str(str2double(ts_sec)+1)];
else
    valid_ts{2} = [file_ts(1:6) num2str(str2double(ts_sec)-1)];
    valid_ts{3} = [file_ts(1:6) num2str(str2double(ts_sec)+1)];
end

if ~isempty(aviFiles)
    for aa = 1:length(aviFiles)
        for vv = 1:3
            tmp_matches(aa,vv) = any(strfind(aviFiles(aa).name,valid_ts{vv}));
        end
    end
end
aviMatchCandidates = sum(tmp_matches,2);
if sum(aviMatchCandidates) > 1
    error('failed to find unique .avi match')
elseif sum(aviMatchCandidates) == 0
    error('failed to find a .avi match')
else
    aviMatchInd = find(aviMatchCandidates);
end
bestMatch.avi = aviFiles(aviMatchInd).name;   
bestMatch.aviPath = fullfile(bonsaiPath,bestMatch.avi);


% --- plot some info about the matching file ---
a = xcorr(trialDur_beh,trialDur_bon_inferred{bestMatch.ind});
figure;
subplot(3,1,1); 
plot(a);
subplot(3,1,2); 
plot(trialDur_bon_inferred{bestMatch.ind}); 
hold on; 
plot(trialDur_beh,'r--')
legend({'trialDuration bonsai','trialDuration beh'})
subplot(3,1,3);
plot(diff(bon.timeStamps));

% --- save plot ---
bestMatch.alignPath = fullfile(bonsaiPath,['align_' behFile '.mat']);
saveas(gcf,fullfile(bonsaiPath,['align_' behFile '.png']));























