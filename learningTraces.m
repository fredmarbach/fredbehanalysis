% plot learning for one mouse
% all traces stacked

clearvars; close all;

antLickWindow = [-1.5 -0.02];
outLickWindow = [-0 0.5];
antLickThreshold = 1;
outLickThreshold = 1;

pre_time = -2;
post_time = 2;
lickBins = pre_time:0.2:post_time;

markerSize = 1;
textSize = 14;
lineWidth = 1;
figureSize = [1 1 7 9];
pbasp = [0.2 1 1];
regColor = [0.3 0.1 0.7];
spacer_trace = 10;
spacer_lick = 2;

mouse = 'mar052';
gSheet = getGSheetString('Experiment Log',mouse);
exps = GetGoogleSpreadsheet(gSheet.DOCID,gSheet.GID);

rootFolder = '/Volumes/winstor/swc/sjones/users/fred/Data_Bpod/';
protocol = 'CuedReward';

types = {'Cue A Reward'};

% fetch file names from google sheet data
tmp = strcmp(exps(1,:),'file name');
filenames = exps(2:end,tmp);
tmp = strcmp(exps(1,:),'date');
filedates = exps(2:end,tmp);
tmp = strcmp(exps(1,:),'includeLearning');
includeSession = strcmp(exps(2:end,tmp),'1');
sessionNames = filenames(includeSession);
sessionFolder = fullfile(rootFolder,mouse,protocol,'Session Data','RewardA','Analysis');
savingFolder = fullfile(rootFolder,'aPlots',mouse);
if ~isdir(savingFolder)
    mkdir(savingFolder)
end

for ss = 1:length(sessionNames)
    % load data
    load(fullfile(sessionFolder,[sessionNames{ss} '_Analysis.mat']));
    
    % find samples pre and post to plot
    timeVec = squeeze(Analysis.Data.Photo_470.relativeTime(1,1,:));
    sampInds = timeVec > pre_time & timeVec < post_time;
    xvec = timeVec(sampInds);
    xvec_licks = lickBins(1:end-1)+0.5*(lickBins(2)-lickBins(1));

    % get logical indices for different trial types (types x trials)
    trialInds = getTrialTypeIndices(Analysis,types);

    % classify trials based on licking
    lickData = Analysis.Data.Licks.Events;
    antLick = sortLicking(antLickWindow,antLickThreshold,lickData);
    outLick = sortLicking(outLickWindow,outLickThreshold,lickData);

    thisTrials = ~antLick.hit.index ...
                 & ~outLick.hit.index ...
                 & trialInds(1,:);
    if sum(thisTrials) > 5
        thisData = squeeze(Analysis.Data.Photo_470.DFF(:,thisTrials,sampInds)); % trials x samples
        thisLicks = Analysis.Data.Licks.Events(thisTrials);
        lickHist = getLickHist(thisLicks,lickBins);

        dataAvg = nanmean(thisData,1);

        subplot(1,2,1)
        plot(xvec,dataAvg+ss*spacer_trace,'Color',regColor,'lineWidth',lineWidth)
        hold on;
        formatFigure(gcf,gca,textSize,lineWidth,0,...
                     'Time from reward [s]','DF/F',[xvec(1)-0.1 xvec(end)+0.1],0,...
                     1,pbasp,figureSize,0,1);

        subplot(1,2,2)
        plot(xvec_licks,lickHist.avg+ss*spacer_lick,'LineWidth',lineWidth,'Color','k');
        hold on;
        formatFigure(gcf,gca,textSize,0,0,...
                     'Time from reward [s]','Licks/s',0,0,...
                     1,pbasp,figureSize,0,1);
    end
end
          

fname = fullfile(savingFolder,[mouse '_learningTraces.pdf']);
saveas(gcf,fname);
 
