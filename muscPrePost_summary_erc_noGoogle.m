% plot pre/post for all muscimol sessions

clearvars; close all;

%%
antLickWindow = [-1.5 -0.02];
outLickWindow = [0 0.3];
antLickThreshold = 1;
outLickThreshold = 1;

binMeasure = 'max';

cueWindow = [-1.5 -1];
rewWindow = [0.1 0.8];
basWindow = 0.05; % how long prior to count as baseline
percSess = [0.1 0.9]; % percentiles of trials to include in mean

sessionShade = 0.85*[1 1 1];
muscShade = [0.9 0.7 0.8];
smoothW = 7;
figureSize = [1 1 8 4];
pbasp = [0.4 1 1];
textSize = 12;
muscColor = [0.8 0.2 0.5];
regColor = [0.3 0.1 0.7];
errColor = 0.7*[1 1 1];

load('Z:\muscimol_tmp.mat');

rootFolder = 'Z:\Data_Bpod\';
protocol = 'CuedReward';

types = {'Cue A Reward' 'Uncued Reward'};

% -------------------------------------------------
% -------------------------------------------------
% fetch file names from google sheet data
tmp = strcmp(exps(1,:),'file name');
filenames = exps(2:end,tmp);
tmp = strcmp(exps(1,:),'includeDLS');
includeSession = strcmp(exps(2:end,tmp),'1');
sessionNames = filenames(includeSession);
savingFolder = fullfile(rootFolder,'aPlots','muscSummary');
if ~isdir(savingFolder)
    mkdir(savingFolder)
end

% get mouse names for axis label
tmp = cellfun(@(x) x(1:6),sessionNames,'UniformOutput',false);
mouseNames = tmp(1:2:end);

% concatenate sessions
[S,sessIndex] = concatSessionsMice(sessionNames,rootFolder);

% get parameters for plotting
stim_duration = S.Properties.GUISettings(1).SoundDuration;
stim_time = S.Data.CueTime(1,1,1);

% get logical indices for different trial types (types x trials)
trialInds = getTrialTypeIndices(S,types);

% classify trials based on licking
lickData = S.Data.Licks.Events;
antLick = sortLicking(antLickWindow,antLickThreshold,lickData);
outLick = sortLicking(outLickWindow,outLickThreshold,lickData);

% get avg values in chosen bins for each trial
allTrials = true(length(sessIndex),1);
% cue
includedTrials{1} = trialInds(1,:) & ...
                 antLick.hit.index & ...
                 outLick.hit.index;
out{1} = getAvgValue(cueWindow,allTrials,S);
bas{1} = getAvgValue([-basWindow 0]+cueWindow(1),allTrials,S);
% cued reward
includedTrials{2} = trialInds(1,:) & ...
                 antLick.hit.index & ...
                 outLick.hit.index;
out{2} = getAvgValue(rewWindow,allTrials,S,outLick.latency);
bas{2} = getAvgValue([-basWindow 0]+rewWindow(1),allTrials,S);
% reward
includedTrials{3} = trialInds(2,:) & ...
                 outLick.hit.index;
out{3} = getAvgValue(rewWindow,allTrials,S,outLick.latency);
bas{3} = getAvgValue([-basWindow 0]+rewWindow(1),allTrials,S);

titles = {'Cue' 'Cued Reward' 'Uncued Reward'};

%%
% get indices for subset of each session, given by percSess
percSessInds = {};
nSessions = max(sessIndex);
for ss = 1:nSessions
    startInd = find(sessIndex==ss,1,'first');
    endInd = find(sessIndex==ss,1,'last');
    inds = round((endInd-startInd)*percSess) + startInd;
    percSessInds{ss} = getLogical(inds(1):inds(2),length(sessIndex));
end

avg = zeros(3,nSessions);
conf95 = zeros(3,nSessions,2);
previousUncuedNorm = 0;
previousData = 0;
prevXval = 0;
pval = zeros(3,nSessions/2);
yval = [];
for ii = 1:3 % loop over cue, reward, uncued reward
    
    figure(1);
    subplot(1,3,ii)
    thisTrace = out{ii}.(binMeasure) - bas{ii}.med; % all trials
    uncuedRew = out{3}.(binMeasure) - bas{3}.med;   % all trials
    
    line([0.65 2.45],[1 1],'Color',regColor,'LineWidth',0.5,'LineStyle','--');
    hold on;
    for ss = 1:nSessions % loop over pre/post pairs
        
        thisInds = percSessInds{ss} & includedTrials{ii};
        thisInds_UR = percSessInds{ss} & includedTrials{3};
        
        uncuedNorm = mean(uncuedRew(thisInds_UR)); % normalise to mean uncued reward
        if ii == 3 && rem(ss,2) == 0 % uncued reward & muscimol session 
            % normalise by previous uncuedNorm
            thisData = thisTrace(thisInds)/previousUncuedNorm;
        else
            thisData = thisTrace(thisInds)/uncuedNorm;
        end
        avg(ii,ss) = mean(thisData);
        conf95(ii,ss,:) = getCI(thisData',0.05,1);
        ci = bootci(1000,@mean,thisData);
        if rem(ss,2) == 1 % odd trials = control
            thisColor = regColor;
            xval = 0.75+ss*0.05;
        else
            thisColor = muscColor;
            xval = 1.75+ss*0.05;
        end
        %errorbar(xval,avg(ii,ss),conf95(ii,ss,1),conf95(ii,ss,2),'Color',thisColor);
        
        line([xval xval],[ci(1) ci(2)],'Color',errColor,'LineWidth',1);
        plot(xval,avg(ii,ss),'o','MarkerFaceColor',thisColor,'Markersize',5,...
                        'MarkerEdgeColor','none')
       
        
        % calculate significance between control/muscimol pairs
        if rem(ss,2) == 0 % even trials = muscimol
            pval(ii,ss/2) = ranksum(thisData,previousData);
            yval(ss/2) = ci(2)+0.2;
            tmp_h = line([prevXval xval],[avg(ii,ss-1) avg(ii,ss)],...
                        'Color',errColor,'LineWidth',1);
            uistack(tmp_h,'bottom')
        end
        previousUncuedNorm = uncuedNorm;
        previousData = thisData;
        prevXval = xval;
    end
        
    title(titles{ii});
    formatFigure(gcf,gca,textSize,2,0,...
                0,0,[0.35 2.65],[0 1.5],...
                1,pbasp,figureSize,0,1);
    ylabel('Normalised DF/F');
    tickvec = [1 2];
    xticks(tickvec);
    xticklabels({'Control' 'Muscimol'});
    %xtickangle(45);
    
    % --- plot avg and sem for each condition ---
    figure(2);
    line([0.9 3.4],[1 1],'Color',regColor,'LineWidth',0.5,'LineStyle','--');
    hold on;
    for dd = 1:2 % loop over pre post muscimol for this trial type
        tmp_data = avg(ii,dd:2:end);
        thisAvg = mean(tmp_data);
        thisSem = std(tmp_data); %/length(avg(ii,dd:2:end));
        thisX = ii+(dd-1)*0.3;
        if dd == 1
            thisColor = regColor;
        else % dd == 2
            thisColor = muscColor;
            [hval,pval] = ttest(tmp_data,tmp_prev,'alpha',0.01)
            tmp_h = line([thisX-0.3 thisX],[mean(tmp_prev) thisAvg],'Color',errColor,'LineWidth',0.5);
            uistack(tmp_h,'bottom');
        end
        plot(thisX,thisAvg,'o','MarkerFaceColor',thisColor,'Markersize',8,...
                            'MarkerEdgeColor','none')
        tmp_h = line([thisX thisX],[-thisSem thisSem]+thisAvg,'Color',errColor,'LineWidth',1);
        uistack(tmp_h,'bottom');
        tmp_prev = tmp_data;
    end
    

end
figure(2);
formatFigure(gcf,gca,textSize,2,0,...
                    e 0,0,[0.9 3.4],[0 1.4],...
                    1,[1 1 1],[2 2 4 4],0,1);
ylabel('Normalised DF/F');
tickvec = [1.15 2.15 3.15];
xticks(tickvec);
xticklabels(titles);
xtickangle(20);

%%
fname = fullfile(savingFolder,'musc_erc_avg.eps');
print(2,'-depsc','-painters',fname) 
fname = fullfile(savingFolder,'musc_erc_avg.pdf');
saveas(2,fname);

fname = fullfile(savingFolder,'musc_erc.eps');
print(1,'-depsc','-painters',fname) 
fname = fullfile(savingFolder,'musc_erc.pdf');
saveas(1,fname);



