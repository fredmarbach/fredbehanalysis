% get the logical indices of trial types
% input: 
%   Analysis - usual data structure
%   varargin{1} - cell array of trialNames; if omitted, all trial types 
%   with non-zero trials are returned
% output:
%   trialInd - logical matrix of trialNames x nTrials

function [trialInds,trialNames] = getTrialTypeIndices(Analysis,varargin)

if nargin > 1
    trialNames = varargin{1};
else 
    tmp = unique(Analysis.Data.TrialTypes);
    trialNames = Analysis.Properties.TrialNames(tmp);
end

trialInds = false(length(trialNames),length(Analysis.Data.TrialTypes));

for ii = 1:length(trialNames)
    thisName = trialNames{ii};
    thisNumber = find(strcmp(Analysis.Properties.TrialNames,thisName));
    trialInds(ii,:) = Analysis.Data.TrialTypes == thisNumber;
end