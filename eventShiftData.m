% extracts a time window of data 'timeWindow' 
% around an event 'eventVec' for each trial
% boundary conditions are handled by padding with NaNs
% input:
%          data - trials x samples
%      eventVec - event time for each trial, in seconds (same as timeVec)
%       timeVec - time points (x axis so to speak)
%    timeWindow - time around event to extract (in seconds)
% output:
%   shiftedData - shifted version of data
%   shiftedTime - new timeVec corresponding to shiftedData with 0 at 

function [shiftedData,shiftedTime] = eventShiftData(data,eventVec,timeVec,timeWindow)

nTrials = size(data,1);

% --- make new time vector ---
samplingRate = 1/median(diff(timeVec));
nSamples_pre = round(timeWindow(1) * samplingRate);
nSamples_post = round(timeWindow(2) * samplingRate);
shiftedTime = (nSamples_pre:nSamples_post-1) / samplingRate;

% --- for each trial, find sample that is closest to event time ---
shiftedData = nan(nTrials,length(shiftedTime));
for ii = 1:nTrials
    tmp_upper = find(timeVec >= eventVec(ii),1,'first');
    tmp_lower = find(timeVec < eventVec(ii),1,'last');
    if (eventVec(ii)-tmp_lower) < (tmp_upper - eventVec(ii))
        % lower sample is closer
        event_samp = tmp_lower;
    else
        % upper sample is closer
        event_samp = tmp_upper;
    end
    
    thisSamples = (nSamples_pre:nSamples_post-1) + event_samp;
    % boundary conditions, nan padding
    lowBoundary = thisSamples < 1;
    highBoundary = thisSamples > size(data,2);
    thisSamples(lowBoundary | highBoundary) = [];
    tmp_inds = sum(lowBoundary)+1:length(shiftedTime)-sum(highBoundary);
    shiftedData(ii,tmp_inds) = data(ii,thisSamples);
end
    

    










