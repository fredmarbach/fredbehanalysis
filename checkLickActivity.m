% plot pre/post for all muscimol sessions

clearvars; close all;

lickWindow(1,:) = [-2.5 -2];
lickWindow(2,:) = [-1.5 -1];
preWindow = -0.5;
lickThreshold1 = 3;
lickThreshold2 = 3;

binMeasure = 'max';

percSess = [0.05 0.8]; % percentiles of trials to include in mean

figureSize = [1 1 6 6];
pbasp = [1 1 1];
textSize = 8;
regColor = [0.3 0.1 0.7];
   
pre_time = -2;
post_time = 2;
lickBins = pre_time:0.2:post_time;

xlim = [pre_time-0.1 post_time+0.1];
lineWidth = 2;

mouse = 'mar065';

rootFolder = '/Volumes/winstor/swc/sjones/users/fred/Data_Bpod/';
protocol = 'CuedReward';

% -------------------------------------------------
% -------------------------------------------------
% fetch file names from google sheet data
gSheet = getGSheetString('Experiment Log',mouse);
exps = GetGoogleSpreadsheet(gSheet.DOCID,gSheet.GID);
tmp = strcmp(exps(1,:),'file name');
filenames = exps(2:end,tmp);
tmp = strcmp(exps(1,:),'includeLicking');
includeSession = strcmp(exps(2:end,tmp),'1');
sessionNames = filenames(includeSession);
savingFolder = fullfile(rootFolder,'aPlots',mouse);
if ~isdir(savingFolder)
    mkdir(savingFolder)
end

% concatenate sessions
[S,sessIndex] = concatSessionsMice(sessionNames,rootFolder);

% get parameters for plotting
stim_duration = S.Properties.GUISettings(1).SoundDuration;
stim_time = S.Data.CueTime(1,1,1);
% find samples pre and post to plot
timeVec = squeeze(S.Data.Photo_470.relativeTime(1,1,:));
xvec_licks = lickBins(1:end-1)+0.5*(lickBins(2)-lickBins(1));

% get logical indices for different trial types (types x trials)
types = {'Cue A Reward' 'Uncued Reward'};
trialInds = getTrialTypeIndices(S,types);

% classify trials based on licking
lickData = S.Data.Licks.Events;
preLick1 = sortLicking([preWindow 0]+lickWindow(1,1),1,lickData);
lick{1} = sortLicking(lickWindow(1,:),lickThreshold1,lickData);
preLick2 = sortLicking([preWindow 0]+lickWindow(2,1),1,lickData);
lick{2} = sortLicking(lickWindow(2,:),lickThreshold2,lickData);

allTrials = true(length(sessIndex),1);
% before cue in all trials
includedTrials{1} = lick{1}.hit.index & ...
                    ~preLick1.hit.index;
% at cue (to see if traces align better to cue or lick)
includedTrials{2} = trialInds(1,:) & ...
                    lick{2}.hit.index & ...
                    ~preLick2.hit.index;

titles = {'before Cue' 'Cue'};

% get indices for subset of each session, given by percSess
percSessInds = {};
nSessions = max(sessIndex);
for ss = 1:nSessions
    startInd = find(sessIndex==ss,1,'first');
    endInd = find(sessIndex==ss,1,'last');
    inds = round((endInd-startInd)*percSess) + startInd;
    percSessInds{ss} = getLogical(inds(1):inds(2),length(sessIndex));
end

%%

for ii = 1:2 % loop over preCue and cue
    
    thisTrials = includedTrials{ii};
    thisData = squeeze(S.Data.Photo_470.DFF(:,thisTrials,:)); % trials x samples
    eventVec = lick{ii}.latency(thisTrials); % latency is in absolute time so this works fine
    [shiftedData,shiftedTime] = eventShiftData(thisData,eventVec,timeVec,[pre_time post_time]);
    % get samples for unshifted data
    tmp_eventVec = lickWindow(ii,1)*ones(1,length(eventVec));
    [normalData,normalTime] = eventShiftData(thisData,tmp_eventVec,timeVec,[pre_time post_time]);
    d1 = nanmean(normalData,1);
    d2 = nanmean(shiftedData,1);
    
    % --- plot traces aligned to cue vs. first lick ---
    subplot(3,2,ii)
    plot([0 0],[min([d1 d2]) max([d1 d2])],'k--','LineWidth',1);
    hold on;
    plot(normalTime,nanmean(normalData,1),'LineWidth',1);
    plot(shiftedTime,nanmean(shiftedData,1),'LineWidth',1)

           
    title(titles{ii});
    formatFigure(gcf,gca,textSize,2,0,...
                'Time from event [s]','DF/F',[pre_time post_time],0,...
                1,pbasp,figureSize,0,1);
    
    % --- plot lick rasters ---        
    licks_shifted = getLickHist(lick{ii}.shifted(thisTrials));
    licks_normal = getLickHist(lickData(thisTrials));
    tickLabels = xticklabels; % get previous plot xticklabels
    tmp = xticks;
    ticks = tmp+lickWindow(ii,1);
    
    subplot(3,2,2+ii)
    plot(licks_normal.lickVec,licks_normal.trialVec,'k.','MarkerSize',3)
    tmp_xlim = [pre_time post_time]+lickWindow(ii,1);
    formatFigure(gcf,gca,textSize,2,0,...
                'Time from event [s]','Trials',tmp_xlim,0,...
                1,pbasp,figureSize,0,1);
    xticks(ticks);
    xticklabels(tickLabels);
    
    subplot(3,2,4+ii)
    plot(licks_shifted.lickVec,licks_shifted.trialVec,'k.','MarkerSize',3)
    formatFigure(gcf,gca,textSize,2,0,...
                'Time from event [s]','Trials',tmp_xlim,0,...
                1,pbasp,figureSize,0,1);
    xticks(ticks);
    xticklabels(tickLabels);
    
end

fname = fullfile(savingFolder,'lickActivity.pdf');
saveas(gcf,fname);
















