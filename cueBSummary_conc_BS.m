% plot learning for one mouse

clearvars; close all;

antLickWindow = [0 0.7]; % relative to cue onset
outLickWindow = [0 0.5]; % relative to reward delivery
rewardWindow = [0 0.5]; % window for binned reward response value
cueWindow = [0 0.5];
antLickThreshold = 1;
outLickThreshold = 1;

pre_time = -2;
post_time = 4;
stim_duration = 0.5;

cueBInd = 1;

markerSize = 1;
textSize = 14;
lineWidth = 3;
figureSize = [1 1 8 4];
stimColor = 0.9*[1 1 1];
avgColor = [0.8 0.2 0.2];

mouse = 'mar069';
gSheet = getGSheetString('Experiment Log',mouse);
exps = GetGoogleSpreadsheet(gSheet.DOCID,gSheet.GID);

rootFolder = '/Volumes/winstor/swc/sjones/users/fred/Data_Bpod/';
protocol = 'BeliefState';

% -------------------------------------------------

% fetch file names from google sheet data
tmp = strcmp(exps(1,:),'file name');
filenames = exps(2:end,tmp);
tmp = strcmp(exps(1,:),'date');
filedates = exps(2:end,tmp);
tmp = strcmp(exps(1,:),'includeCueB');
includeSession = strcmp(exps(2:end,tmp),'1');
sessionNames = filenames(includeSession);
sessionFolder = fullfile(rootFolder,mouse,protocol,'Session Data','Analysis');
savingFolder = fullfile(rootFolder,'aPlots',mouse,'summary');
if ~isdir(savingFolder)
    mkdir(savingFolder)
end

% concatenate sessions
[S,sessIndex] = concatSessionsMice(sessionNames,rootFolder);
    
% find samples pre and post to plot
timeVec = squeeze(S.Data.Photo_470.relativeTime(1,1,:));
sampInds = timeVec > pre_time & timeVec < post_time;
xvec = timeVec(sampInds);

% get logical indices for different trial types (types x trials)
[trialInds,trialNames] = getTrialTypeIndices(S);
uncuedInd = find(strcmp(trialNames,'Uncued Reward'));

% --- plot: each trial type as image plot in a row ---
nTrials = sum(trialInds,2);
maxTrials = max(nTrials);
nTypes = size(trialInds,1);

% --- deal with variable cue B ---

% classify trials based on licking
lickData = S.Data.Licks.Events;
delays = round(S.Data.OutcomeTime,2);
antLick = sortLicking(antLickWindow,antLickThreshold,lickData);
% get reward response for all trials, baseline subtracted
tmp_reward = getAvgValue(rewardWindow,1,S,delays);
tmp_baseline = getAvgValue([-0.06 0],1,S,delays);
rewardResponse = tmp_reward.max - tmp_baseline.avg;
% licking: deal with variable reward time
tmp = repmat(outLickWindow,length(delays),1);
timeWindow = bsxfun(@plus,tmp,delays');
outLick = sortLicking(timeWindow,outLickThreshold,lickData);

% == plot histogram of cue B delay type trial numbers ==
subplot(1,3,1)
cueBTrials = trialInds(cueBInd,:) & antLick.hit.index & outLick.hit.index;
delaysB = delays(cueBTrials);
uniqueDelays = unique(delaysB);
if length(uniqueDelays) > 1
    delayStep = median(diff(uniqueDelays));
    edgesB = [-delayStep+uniqueDelays(1) uniqueDelays] + delayStep*0.5;
    subplot(1,3,1)
    histogram(delaysB,edgesB)
    formatFigure(gcf,gca,8,0,0,...
                   'Delay from cue onset [s]','Counts',0,0,...
                   1,[1 1 1],figureSize,0,1);
    xticks(uniqueDelays);
end

% == plot avg traces ==
thisData = squeeze(S.Data.Photo_470.DFF(:,cueBTrials,sampInds)); % trials x samples
subplot(1,3,2)
for dd = 1:length(uniqueDelays)
    tmp = thisData(delaysB == uniqueDelays(dd),:);
    plot(xvec,nanmean(tmp,1))
    hold on;
end
formatFigure(gcf,gca,8,0,0,...
             'Time from cue [s]','DF/F',0,0,...
             1,[1 1 1],figureSize,0,1);

% == plot binned avg/std ==
% uncued reward
subplot(1,3,3)
thisX = [-1.8 -1.3 -0.9 -0.3 uniqueDelays];
meanDataPoints = zeros(1,length(thisX));
tmp = arrayfun(@(x) ['delay ' num2str(x)],uniqueDelays,'UniformOutput',false);
thisXLabels = {'Reward' 'CueA' 'CueA R' 'CueB' tmp{1:end}};

uncuedTrials = trialInds(uncuedInd,:) & outLick.hit.index;
uncuedData = rewardResponse(uncuedTrials);
UR_avg = nanmean(uncuedData);
meanDataPoints(1) = UR_avg/UR_avg;
UR_std = nanstd(uncuedData/UR_avg);
errorbar(thisX(1),meanDataPoints(1),UR_std)
hold on;

% cue A
cueATrials = trialInds(1,:) & antLick.hit.index & outLick.hit.index;
tmp_cueA = getAvgValue(cueWindow,cueATrials,S);
tmp_baseline = getAvgValue([-0.06 0],cueATrials,S);
cueAResponse = tmp_cueA.max - tmp_baseline.avg;
meanDataPoints(2) = nanmean(cueAResponse/UR_avg);
A_std = nanstd(cueAResponse/UR_avg);
errorbar(thisX(2),meanDataPoints(2),A_std)

% cue A reward
cueAReward = rewardResponse(cueATrials);
meanDataPoints(3) = nanmean(cueAReward/UR_avg);
AR_std = nanstd(cueAReward/UR_avg);
errorbar(thisX(3),meanDataPoints(3),AR_std)

% cue B
tmp_cueB = getAvgValue(cueWindow,cueBTrials,S);
tmp_baseline = getAvgValue([-0.06 0],cueBTrials,S);
cueBResponse = tmp_cueB.max - tmp_baseline.avg;
meanDataPoints(4) = nanmean(cueBResponse/UR_avg);
B_std = nanstd(cueBResponse/UR_avg);
errorbar(thisX(4),meanDataPoints(4),B_std)

% cue B reward
for dd = 1:length(uniqueDelays)
    tmp_inds = delays == uniqueDelays(dd) & cueBTrials;
    meanDataPoints(4+dd) = nanmean(rewardResponse(tmp_inds)/UR_avg);
    errorbar(thisX(4+dd),meanDataPoints(4+dd),nanstd(rewardResponse(tmp_inds)/UR_avg))
end
plot(thisX,meanDataPoints,'o','MarkerFaceColor',[0.5 0.5 0.5],'Markersize',5,...
                    'MarkerEdgeColor','none')
hold off;
xticks(thisX);
xticklabels(thisXLabels);
xtickangle(45);
formatFigure(gcf,gca,8,0,0,...
             'Bin type','max DF/F',[thisX(1)-0.2 thisX(end)+0.2],[-0.2 1.5],...
             1,[1 1 1],figureSize,0,1);

fname = fullfile(savingFolder,[mouse '_cueA_23.pdf']);
saveas(gcf,fname);
clf;









