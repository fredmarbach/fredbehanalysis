% count and threshold licks in a given window
% inputs:
%        timeWindow - [start end] in seconds; 
%                     or array of trials x 2
%   nLicksThreshold - licks within window to count as a 'hit'
%          lickData - cell array over trials of lick time stamps
%     trialIndexVec - logical indices of trials of lickData to consider
%                     default uses all trials
%           edgeVec - edges for histogram in seconds (default -4:0.25:4)
% output:
%      licks.hit.index - logicals of trials with licks >= nLicksThreshold
%    licks.hit.nTrials - number of 'hit' trials
% licks.hit.binCenters - useful for plotting
%    licks.hit.edgeVec - same as varargin{2} / default
%  licks.hit.histogram - counts per bin in edgeVec
%          licks.miss. -- all same as above
%        licks.latency - time stamp of first lick within given window, absolute time
%        licks.shifted - lick timestamps with latency on each trial subtracted
%       licks.trialVec - trial index for each lick timestamp, e.g. [1 1 1 2 2 3 4 4]
%        licks.lickVec - lick timestamps in linear form
%licks.lickVec_shifted - lickVec with each trial shifted by latency

function [licks] = sortLicking(timeWindow,nLicksThreshold,lickData,varargin)

% if not given as input, create logical trial index vector
if nargin > 3
    trialIndexVec = varargin{1};
    if nargin > 4
        edgeVec = varargin{2};
        binSize = edgeVec(2)-edgeVec(1);
    end
else
    trialIndexVec = true(length(lickData),1);
    binSize = 0.25; % histogram bin size
    edgeVec = -4:binSize:4;
end

trialNumbers = find(trialIndexVec);
nTrials = length(trialNumbers);

% prepare timeWindow array
if size(timeWindow,1) == 1
    timeWindow = repmat(timeWindow,nTrials,1);
end

% initialise
licks.hit.index = false(length(trialIndexVec),1);
licks.miss.index = false(length(trialIndexVec),1);
licks.nLicks = zeros(nTrials,1);
licks.shifted = {};

% loop over trials and count licks etc.
for tt = 1:nTrials
    thisTrial = trialNumbers(tt);
    thisLicks = lickData{thisTrial};
    thisNLicks = sum(thisLicks > timeWindow(nTrials,1) & ...
                     thisLicks < timeWindow(nTrials,2));
    
    licks.hit.index(thisTrial) = thisNLicks >= nLicksThreshold;
    licks.miss.index(thisTrial) = ~licks.hit.index(thisTrial);
    licks.nLicks(tt) = thisNLicks;
    
    % store time stamp of first lick inside timeWindow
    if thisNLicks > 0
        tmp = find(thisLicks > timeWindow(nTrials,1),1,'first');
        licks.latency(tt) = thisLicks(tmp);
        licks.shifted{tt} = thisLicks - licks.latency(tt) + timeWindow(nTrials,1);
    else
        licks.latency(tt) = NaN;
        licks.shifted{tt} = thisLicks; % not sure this is smart, but usually those trials will not be looked anyways since there are no licks
    end
    trialVec{tt} = tt*ones(1,length(thisLicks)); % for raster plot
end

licks.hit.nTrials = sum(licks.hit.index);
licks.miss.nTrials = sum(licks.miss.index);

% useful to have centers for plotting
licks.hit.binCenters = edgeVec(1:end-1) + binSize/2;
licks.miss.binCenters = licks.hit.binCenters;

licksHit = [lickData{licks.hit.index}];
licksMiss = [lickData{licks.miss.index}];

[licks.hit.histogram,licks.hit.edgeVec] = histcounts(licksHit,edgeVec);
[licks.miss.histogram,licks.miss.edgeVec] = histcounts(licksMiss,edgeVec);

% convert to Hz
licks.hit.histogram = licks.hit.histogram / licks.hit.nTrials / binSize;
licks.miss.histogram = licks.miss.histogram / licks.miss.nTrials / binSize;

% flip dimensions
% because most other indexing vectors I use are row vectors
licks.hit.index = licks.hit.index';
licks.miss.index = licks.miss.index';

% make a cell array with trial numbers
% useful for lick raster plot
licks.trialVec = cell2mat(trialVec);
licks.lickVec_shifted = cell2mat(licks.shifted);
licks.lickVec = cell2mat(lickData);






