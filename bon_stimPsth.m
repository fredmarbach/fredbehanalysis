% load the align__.mat output file of align_bonsai_beh.m
% plot event-aligned trialtype psth for all video rois

%%
clearvars

%% user defined parameters
behFile = 'mar082_CuedReward_Apr23_2019_Session5'; 
%behRoot = 'Z:\users\fred\Data_Bpod';
behRoot = 'Z:\Data_Bpod';

timePrePost = [-3 3];
stimTime = [-1.5 -1];

% --- load align__.mat file ---
% loads structs bestMatch and frameInfo
p = getBehFileInfo(behFile);
load(fullfile(behRoot,p.mouse,'Bonsai Videos',['align_' behFile]));
savePath = fullfile(behRoot,'aPlots',p.mouse,behFile);
if ~exist(savePath)
    mkdir(savePath);
end

%% get frame indices to plot
% == check frameInfo.trialTypes for valid trialTypes ==
%trialTypes = {'Cue A Reward' 'Cue B Omission' 'Uncued Reward'};

alignTo = 'outcome'; % 'stimOn' 'stimOff' 'outcome' 'lick'

trialTypeNum = unique(bon.trialTypes);
nTypes = length(trialTypeNum);
trialTypes = bon.trialNames(trialTypeNum);

framesPrePost = round(bon.rate_median * timePrePost);
xvec = (framesPrePost(1):framesPrePost(2)) / bon.rate_median;

% indMat is a cell array trialTypes x planes
[indMat,alignFrames] = getFrameIndMat_bon(bon,trialTypes,alignTo,framesPrePost);

       
% plot sorted image plot of avg for each roi
nRois = length(bon.rois);
for rr = 1:nRois
    figure;
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.24, 1, 0.76]);
    
    for ii = 1:2
        if ii == 1
            thisRoiTrace = bon.rois(rr).diff;
        else
            thisRoiTrace = bon.rois(rr).mean;
        end
        for tt = 1:nTypes

            tmp_data = thisRoiTrace(indMat{tt});
            meanData = mean(tmp_data,1);

            subplot(2,nTypes,(ii-1)*nTypes+tt)
            line([stimTime(1) stimTime(1)],[min(tmp_data(:)) max(tmp_data(:))])
            hold on;
            line([0 0],[min(tmp_data(:)) max(tmp_data(:))])
            plot(xvec,tmp_data,'Color',0.7*[1 1 1])
            plot(xvec,meanData,'Color','k','LineWidth',2)
            hold off;
            axis tight;

            title(trialTypes{tt})
            formatFigure(gcf,gca,12,0,0,...
                         'Time [s]','ROI number (sorted)',0,0,...
                         1,[1 1 1],0,0,1);
        end
    end
    % --- save for each roi ---
    fname = fullfile(savePath,['bon_roi' num2str(rr) '.png']);
    saveas(gcf,fname);
    %print(gcf,'-depsc','-painters',fname) 
end


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
            
            
            
            