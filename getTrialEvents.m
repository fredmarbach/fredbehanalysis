% extract basic behavior events from behavior file

function ev = getTrialEvents(behData,trialNumber)

eventNames = {'stimOn' 'stimOff' 'outcome' 'lick'};


trialEvents = behData.RawEvents.Trial{trialNumber}.Events;
trialStates = behData.RawEvents.Trial{trialNumber}.States;
stimDuration = behData.TrialSettings(1).GUI.SoundDuration;
% stimOn, stimOff
if isfield(trialEvents,'BNC2High')
    ev.(eventNames{1}) = trialEvents.BNC2High;
    ev.(eventNames{2}) = trialEvents.BNC2High + stimDuration;
elseif ~isempty(trialStates.SoundDelivery)
    ev.(eventNames{1}) = trialStates.SoundDelivery(1);
    ev.(eventNames{2}) = trialStates.SoundDelivery(1) + stimDuration;
else
    ev.(eventNames{1}) = [];
    ev.(eventNames{2}) = [];
end
% reward
ev.(eventNames{3}) = trialStates.Outcome(1);
% lick
if isfield(trialEvents,'Port1In')
    ev.(eventNames{4}) = trialEvents.Port1In;
else
    ev.(eventNames{4}) = [];
end