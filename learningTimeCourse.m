% plot learning for one mouse

clearvars; close all;

antLickWindow = [-1.5 -0.02];
outLickWindow = [0 0.5];
antLickThreshold = 1;
outLickThreshold = 1;

cueWindow = [-1.5 -1];
rewWindow = [0 0.4];
basWindow = 0.05; % how long prior to count as baseline
percSess = [0.05 0.7]; % percentiles of trials to include in mean

sessionShade = 0.85*[1 1 1];
muscShade = [0.9 0.7 0.8];
smoothW = 7;
figureSize = [1 1 9 7];
textSize = 8;
muscColor = [0.8 0.2 0.5];
regColor = [0.3 0.1 0.7];

mouse = 'mar066';
gSheet = getGSheetString('Experiment Log',mouse);
exps = GetGoogleSpreadsheet(gSheet.DOCID,gSheet.GID);

rootFolder = '/Volumes/winstor/swc/sjones/users/fred/Data_Bpod/';
protocol = 'CuedReward';

types = {'Cue A Reward' 'Uncued Reward'};

% -------------------------------------------------
% -------------------------------------------------
% fetch file names from google sheet data
tmp = strcmp(exps(1,:),'file name');
filenames = exps(2:end,tmp);
tmp = strcmp(exps(1,:),'include');
includeSession = strcmp(exps(2:end,tmp),'1');
sessionNames = filenames(includeSession);
sessionFolder = fullfile(rootFolder,mouse,protocol,'Session Data','RewardA','Analysis');
savingFolder = fullfile(rootFolder,'aPlots',mouse,'learning');
if ~isdir(savingFolder)
    mkdir(savingFolder)
end

% identify possible muscimol sessions
% makes sure the 'muscSession' indices refer to the concatenated dataset
% where some sessions have been omitted
tmp = strcmp(exps(1,:),'muscimol');
tmp = strcmp(exps(2:end,tmp),'1');
includeSession_ind = cumsum(includeSession).*includeSession.*tmp;
muscSession = includeSession_ind(includeSession_ind>0); 


% concatenate sessions
[S,sessIndex] = concatSessions(sessionNames,sessionFolder);

% get parameters for plotting
stim_duration = S.Properties.GUISettings(1).SoundDuration;
stim_time = S.Data.CueTime(1,1,1);

% get logical indices for different trial types (types x trials)
trialInds = getTrialTypeIndices(S,types);

% classify trials based on licking
lickData = S.Data.Licks.Events;
antLick = sortLicking(antLickWindow,antLickThreshold,lickData);
outLick = sortLicking(outLickWindow,outLickThreshold,lickData);

% get avg values in chosen bins for each trial
% cue
includedTrials{1} = trialInds(1,:) & ...
                 antLick.hit.index & ...
                 outLick.hit.index;
out{1} = getAvgValue(cueWindow,includedTrials{1},S);
bas{1} = getAvgValue([-basWindow 0]+cueWindow(1),includedTrials{1},S);
% cued reward
includedTrials{2} = trialInds(1,:) & ...
                 antLick.hit.index & ...
                 outLick.hit.index;
out{2} = getAvgValue(rewWindow,includedTrials{2},S,outLick.latency);
bas{2} = getAvgValue([-basWindow 0]+rewWindow(1),includedTrials{2},S);
% reward
includedTrials{3} = trialInds(2,:) & ...
                 outLick.hit.index;
out{3} = getAvgValue(rewWindow,includedTrials{3},S,outLick.latency);
bas{3} = getAvgValue([-basWindow 0]+rewWindow(1),includedTrials{3},S);

titles = {'Cue' 'Cued Reward' 'Reward'};

%%
outFields = fieldnames(out{1});
for oo = 1:length(outFields) % loop over avg,med,max
    for bb = 1:2 % loop over subtracting baseline or not
        for ii = 1:3 % loop over cue, reward, uncued reward
            % --- plot each trial ---
            subplot(3,2,2*ii-1)
            if bb == 1
                thisTrace = out{ii}.(outFields{oo});
                uncuedRew = out{3}.(outFields{oo});
            else
                thisTrace = out{ii}.(outFields{oo}) - bas{ii}.med;
                uncuedRew = out{3}.(outFields{oo}) - bas{3}.med;
            end
            xExtent = length(thisTrace);
            yExtent = max(thisTrace)-min(thisTrace);

            sessionNumber = sessIndex(includedTrials{ii});
            sessionInds = unique(sessionNumber);
            nSessions = length(sessionInds);
            sessionChange = [1 find(diff(sessionNumber))+1 xExtent];
            plot([0 xExtent],[0 0],'k--','linewidth',1)
            hold on;
            plot(thisTrace)
            plot(filtfilt(ones(1,smoothW)/smoothW,1,thisTrace),...
                                    'linewidth',2,'Color',regColor);

            % --- plot shaded rectangles ---                    
            % shade muscimol sessions
            rh = [];
            for ss = 1:length(muscSession)
                rh(ss) = rectangle('Position',[sessionChange(muscSession(ss)) ...
                                           min(thisTrace+0.05*yExtent) ...
                                           sessionChange(muscSession(ss)+1)-sessionChange(muscSession(ss)) ...
                                           yExtent-0.05*2*yExtent],...
                               'FaceColor',muscShade,'EdgeColor','none');
                uistack(rh(ss),'bottom');
            end
            % shade alternated sessions
            rh = [];    
            for ss = 1:2:length(sessionChange)-1
                rh(ss) = rectangle('Position',[sessionChange(ss) ...
                                           min(thisTrace+0.05*yExtent) ...
                                           sessionChange(ss+1)-sessionChange(ss) ...
                                           yExtent-0.05*2*yExtent],...
                               'FaceColor',sessionShade,'EdgeColor','none');
                uistack(rh(ss),'bottom');
            end
                
            axis tight
            title(titles{ii});
            formatFigure(gcf,gca,textSize,0,0,...
                           'Trial Number','DF/F',0,0,...
                           1,0,figureSize,1,1);
            
            % --- plot mean/std per session ---
            subplot(3,2,2*ii);
            % make actual trial numbers of thisTrace trials in full length includedTrials
            trials2includedTrials = cumsum(includedTrials{ii}).*includedTrials{ii};
            trials2includedTrials_uncuedRew = cumsum(includedTrials{3}).*includedTrials{3};
            for ss = 1:nSessions
                startInd = find(sessIndex==sessionInds(ss),1,'first');
                endInd = find(sessIndex==sessionInds(ss),1,'last');
                thisInds = round((endInd-startInd)*percSess) + startInd;
                tmp_inds = trials2includedTrials(thisInds(1):thisInds(2));
                tmp_inds(tmp_inds==0) = [];
                tmp_inds_UR = trials2includedTrials_uncuedRew(thisInds(1):thisInds(2));
                tmp_inds_UR(tmp_inds_UR==0) = [];
                uncuedNorm = mean(uncuedRew(tmp_inds_UR)); % normalise to mean uncued reward
                avg = mean(thisTrace(tmp_inds)/uncuedNorm);
                stdev = std(thisTrace(tmp_inds)/uncuedNorm);
                %ci = bootci(1000,@mean,thisTrace(tmp_inds));
                conf95 = getCI(thisTrace(tmp_inds)'/uncuedNorm,0.05,1);
                if ismember(ss,muscSession)
                    thisColor = muscColor;
                else
                    thisColor = regColor;
                end
                errorbar(ss,avg,conf95(1),conf95(2),'Color',thisColor);
                hold on;
                plot(ss,avg,'o','MarkerFaceColor',thisColor,'Markersize',5,...
                            'MarkerEdgeColor','none')
                
            end
            title(titles{ii});
            formatFigure(gcf,gca,textSize,2,0,...
                           'Session Number','Normalised DF/F',[0 nSessions]+0.5,0,...
                           1,0,figureSize,1,1);
            
        end

        if bb == 1
            fname = fullfile(savingFolder,[mouse '_' outFields{oo} '_learning.pdf']);
        else
            fname = fullfile(savingFolder,[mouse '_' outFields{oo} '_learningBS.pdf']);
        end
        saveas(gcf,fname);
        clf;
    end
end






