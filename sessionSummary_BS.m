% plot learning for one mouse

clearvars; close all;

antLickWindow = [0 0.7]; % relative to cue onset
outLickWindow = [0 0.5]; % relative to reward delivery
antLickThreshold = 1;
outLickThreshold = 1;

pre_time = -2;
post_time = 4;
stim_duration = 0.5;

markerSize = 1;
textSize = 14;
lineWidth = 3;
figureSize = [1 1 8 4];
stimColor = 0.9*[1 1 1];
avgColor = [0.8 0.2 0.2];

mouse = 'mar073';
gSheet = getGSheetString('Experiment Log',mouse);
exps = GetGoogleSpreadsheet(gSheet.DOCID,gSheet.GID);

rootFolder = '/Volumes/winstor/swc/sjones/users/fred/Data_Bpod/';
protocol = 'CuedReward';

% -------------------------------------------------

% fetch file names from google sheet data
tmp = strcmp(exps(1,:),'file name');
filenames = exps(2:end,tmp);
tmp = strcmp(exps(1,:),'date');
filedates = exps(2:end,tmp);
%tmp = strcmp(exps(1,:),'include');
%includeSession = strcmp(exps(2:end,tmp),'1');
sessionNames = filenames; %filenames(includeSession);
sessionFolder = fullfile(rootFolder,mouse,protocol,'Session Data','Analysis');
savingFolder = fullfile(rootFolder,'aPlots',mouse,'summary');
if ~isdir(savingFolder)
    mkdir(savingFolder)
end

% -------------------
% loop over sessions
% -------------------
for ss = 1:length(sessionNames)
    % load data
    load(fullfile(sessionFolder,[sessionNames{ss} '_Analysis.mat']));
    
    % find samples pre and post to plot
    timeVec = squeeze(Analysis.Data.Photo_470.relativeTime(1,1,:));
    sampInds = timeVec > pre_time & timeVec < post_time;
    xvec = timeVec(sampInds);

    % get logical indices for different trial types (types x trials)
    [trialInds,trialNames] = getTrialTypeIndices(Analysis);
    
    % --- plot: each trial type as image plot in a row ---
    nTrials = sum(trialInds,2);
    maxTrials = max(nTrials);
    nTypes = size(trialInds,1);
    for tt = 1:nTypes
        thisTrials = trialInds(tt,:);
        % image plot
        subplot(2,nTypes*2,2*tt-1)
        thisData = squeeze(Analysis.Data.Photo_470.DFF(:,thisTrials,sampInds)); % trials x samples
        imagesc(thisData);
        ylim([0 maxTrials])
        title(trialNames{tt});
        
        % lick plot
        subplot(2,nTypes*2,2*tt);
        thisLicks = Analysis.Data.Licks.Events(thisTrials);
        out = getLickHist(thisLicks);

        plot(out.lickVec,out.trialVec,...
             'ks','MarkerFaceColor','k','MarkerSize',markerSize)
        set(gca,'Ydir','reverse')
        xlim([xvec(1) xvec(end)])
        ylim([0 maxTrials])
        
        if sum(thisTrials) > 3
            % plot activity psth underneith
            subplot(2,nTypes*2,nTypes*2+2*tt-1);
            dataAvg = nanmean(thisData,1);
            % get confidence intervals for shaded error bar
            ci = bootci(500,@nanmean,thisData);
            if ~strcmp(trialNames{tt},'Uncued Reward')
                rh = rectangle('Position',[0 min(ci(:)) stim_duration abs(min(ci(:)))+max(ci(:))],...
                           'FaceColor',stimColor,'EdgeColor','none');
            end
            hold on;
            shadedErrorBar(xvec,dataAvg,[ci(2,:)-dataAvg;dataAvg-ci(1,:)],...
                            {'LineWidth',lineWidth,'color',avgColor},1);
            xlim([xvec(1) xvec(end)])
        end
    end
    formatFigure(gcf,gca,0,0,0,...
                   0,0,0,0,...
                   0,0,figureSize,0,1);
    fname = fullfile(savingFolder,[mouse '_' filedates{ss} '.pdf']);
    saveas(gcf,fname);
    clf;
    
    % --- deal with variable cue B ---
    
    % classify trials based on licking
    lickData = Analysis.Data.Licks.Events;
    delays = round(Analysis.Data.OutcomeTime,2);
    antLick = sortLicking(antLickWindow,antLickThreshold,lickData);
    % deal with variable reward time
    tmp = repmat(outLickWindow,length(delays),1);
    timeWindow = bsxfun(@plus,tmp,delays');
    outLick = sortLicking(timeWindow,outLickThreshold,lickData);
    thisTrials = trialInds(2,:) & ...
                 antLick.hit.index & ...
                 outLick.hit.index;
                 
    % plot histogram of delay types
    delaysB = delays(thisTrials);
    uniqueDelays = unique(delaysB);
    if length(uniqueDelays) > 1
        delayStep = median(diff(uniqueDelays));
        edgesB = [-delayStep+uniqueDelays(1) uniqueDelays] + delayStep*0.5;
        subplot(1,2,1)
        histogram(delaysB,edgesB)
        formatFigure(gcf,gca,8,0,0,...
                       'Delay from cue onset [s]','Counts',0,0,...
                       1,[1 1 1],figureSize,0,1);
        xticks(uniqueDelays);
    end
    
    % plot avg traces
    thisData = squeeze(Analysis.Data.Photo_470.DFF(:,thisTrials,sampInds)); % trials x samples
    subplot(1,2,2)
    for dd = 1:length(uniqueDelays)
        tmp = thisData(delaysB == uniqueDelays(dd),:);
        plot(xvec,nanmean(tmp,1))
        hold on;
    end
    formatFigure(gcf,gca,8,0,0,...
                 'Time from cue [s]','DF/F',0,0,...
                 1,[1 1 1],figureSize,0,1);
    fname = fullfile(savingFolder,[mouse '_cueB_' filedates{ss} '.pdf']);
    saveas(gcf,fname);
    clf;
    
    % plot avg/std per delay
    % normalise by uncued reward
    uncuedTrials = trialInds(4,:) & antLick.hit.index & outLick.hit.index;
    uncuedData = squeeze(Analysis.Data.Photo_470.DFF(:,uncuedTrials,:));
    uncuedInds = timeVec > 0.5 & timeVec < 1;
    UR_mean = nanmean(max(uncuedData(:,uncuedInds),[],2));
    UR_std = nanstd(max(uncuedData(:,uncuedInds)/UR_mean,[],2));
    subplot(1,2,2)
    errorbar(-1,1,UR_std)
    hold on;
    plot(-1,1,'o','MarkerFaceColor',[0.5 0.5 0.5],'Markersize',5,...
                        'MarkerEdgeColor','none')
    % cue A
    cueATrials = trialInds(1,:) & antLick.hit.index & outLick.hit.index;
    cueAData = squeeze(Analysis.Data.Photo_470.DFF(:,cueATrials,:));
    cueADelay = median(delays(cueATrials));
    cueAInds = timeVec > cueADelay+stim_duration & timeVec < cueADelay+stim_duration+0.5;
    A_mean = nanmean(max(cueAData(:,cueAInds)/UR_mean,[],2));
    A_std = nanstd(max(cueAData(:,cueAInds)/UR_mean,[],2));
    subplot(1,2,2)
    errorbar(0,A_mean,UR_std)
    hold on;
    plot(0,A_mean,'o','MarkerFaceColor',[0.5 0.5 0.5],'Markersize',5,...
                        'MarkerEdgeColor','none')
    
    for dd = 1:length(uniqueDelays)
        tmp_inds = xvec > uniqueDelays(dd)+stim_duration & xvec < uniqueDelays(dd)+stim_duration+0.5;
        tmp = thisData(delaysB == uniqueDelays(dd),tmp_inds);
        maxResponse = nanmean(max(tmp/UR_mean,[],2));
        stdResponse = nanstd(max(tmp/UR_mean,[],2));
        errorbar(dd,maxResponse,stdResponse)
        hold on;
        plot(dd,maxResponse,'o','MarkerFaceColor',[0.5 0.5 0.5],'Markersize',5,...
                        'MarkerEdgeColor','none')
    end
    hold off;
    formatFigure(gcf,gca,8,0,0,...
                 'Delay','max DF/F',[-1.3 length(uniqueDelays)+2+0.3],0,...
                 1,[1 1 1],figureSize,0,1);
    fname = fullfile(savingFolder,[mouse '_cueB_bins_' filedates{ss} '.pdf']);
    saveas(gcf,fname);
    clf;
    
end








