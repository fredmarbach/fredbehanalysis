% plot learning time course over sessions for a selection of mice

clearvars; close all;

antLickWindow = [-1.5 -0.02];
outLickWindow = [0 0.5];
antLickThreshold = 1;
outLickThreshold = 1;

cueWindow = [-1.5 -1];
rewWindow = [0 0.4];
basWindow = 0.05; % how long prior to count as baseline
percSess = [0.05 0.7]; % percentiles of trials to include in mean
binMeasure = 'max';
includeTag = 'includeLearningMice';

mice = {'mar036' 'mar037' 'mar038' 'mar052' 'mar053' ...
        'mar065' 'mar066'};
lesionIndex = [0 0 0 0 0 1 1];

rootFolder = '/Volumes/winstor/swc/sjones/users/fred/Data_Bpod/';
protocol = 'CuedReward';
types = {'Cue A Reward' 'Uncued Reward'};
savingFolder = fullfile(rootFolder,'aPlots','learningSummary');
if ~isdir(savingFolder)
    mkdir(savingFolder)
end
     

nMice = length(mice);
tracesMice = {};
for mm = 1:nMice
    % fetch file names from google sheet data
    gSheet = getGSheetString('Experiment Log',mice{mm});
    exps = GetGoogleSpreadsheet(gSheet.DOCID,gSheet.GID);

    tmp = strcmp(exps(1,:),'file name');
    filenames = exps(2:end,tmp);
    tmp = strcmp(exps(1,:),includeTag);
    includeSession = strcmp(exps(2:end,tmp),'1');
    sessionNames = filenames(includeSession);
    sessionFolder = fullfile(rootFolder,mice{mm},protocol,'Session Data','RewardA','Analysis');

    % concatenate sessions
    [S,sessIndex] = concatSessions(sessionNames,sessionFolder);

    % get indices for subset of each session, given by percSess
    percSessInds = {};
    nSessions = max(sessIndex);
    for ss = 1:nSessions
        startInd = find(sessIndex==ss,1,'first');
        endInd = find(sessIndex==ss,1,'last');
        inds = round((endInd-startInd)*percSess) + startInd;
        percSessInds{ss} = getLogical(inds(1):inds(2),length(sessIndex));
    end
    
    % get logical indices for different trial types (types x trials)
    trialInds = getTrialTypeIndices(S,types);

    % classify trials based on licking
    lickData = S.Data.Licks.Events;
    antLick = sortLicking(antLickWindow,antLickThreshold,lickData);
    outLick = sortLicking(outLickWindow,outLickThreshold,lickData);

    % get avg values in chosen bins for each trial
    allTrials = true(length(sessIndex),1);
    % cue
    includedTrials{1} = trialInds(1,:) & ...
                     antLick.hit.index & ...
                     outLick.hit.index;
    out{1} = getAvgValue(cueWindow,allTrials,S);
    bas{1} = getAvgValue([-basWindow 0]+cueWindow(1),allTrials,S);
    % cued reward
    includedTrials{2} = trialInds(1,:) & ...
                     antLick.hit.index & ...
                     outLick.hit.index;
    out{2} = getAvgValue(rewWindow,allTrials,S,outLick.latency);
    bas{2} = getAvgValue([-basWindow 0]+rewWindow(1),allTrials,S);
    % reward
    includedTrials{3} = trialInds(2,:) & ...
                     outLick.hit.index;
    out{3} = getAvgValue(rewWindow,allTrials,S,outLick.latency);
    bas{3} = getAvgValue([-basWindow 0]+rewWindow(1),allTrials,S);

    for ii = 1:3 % loop over cue, reward, uncued reward
    
        thisTrace = out{ii}.(binMeasure) - bas{ii}.med; % all trials
        uncuedRew = out{3}.(binMeasure) - bas{3}.med;   % all trials
        
        for ss = 1:nSessions % loop over pre/post pairs
            
            thisInds = percSessInds{ss} & includedTrials{ii};
            thisInds_UR = percSessInds{ss} & includedTrials{3};
            
            uncuedNorm = mean(uncuedRew(thisInds_UR)); % normalise to mean uncued reward
            thisData = thisTrace(thisInds)/uncuedNorm;
            tracesMice{mm}(ii,ss) = mean(thisData);
            
        end
    end
end
    
%% plot traces of all mice
figureSize = [1 1 7 5];
textSize = 10;
lesionColor = [0.8 0.2 0.5];
regColor = [0.3 0.1 0.7];
titles = {'Cue' 'Cued Reward' 'Reward'};
for mm = 1:nMice
    for ii = 1:2 % loop over cue and cued reward time bins
           subplot(1,2,ii);
           if lesionIndex(mm) == 0
               thisColor = regColor;
           else
               thisColor = lesionColor;
           end
           plot(tracesMice{mm}(ii,:),'Color',thisColor,'linewidth',1.5)
           hold on;
           title(titles{ii});
           formatFigure(gcf,gca,textSize,2,0,...
                'Session Number','Normalised DF/F',0,0,...
                1,0,figureSize,1,1);
    end
end

% save pdf
fname = fullfile(savingFolder,'learningMice.pdf');
saveas(gcf,fname);





