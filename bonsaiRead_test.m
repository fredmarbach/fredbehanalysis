%% 

clearvars

%%
rootDir = 'Z:\Data_Bpod\mar082\Bonsai Videos';
allFiles = dir([rootDir filesep '*.avi']);
nFiles = length(allFiles);

behFile = dir([rootDir filesep '*.mat']);
load(fullfile(rootDir,behFile(1).name));
behData = SessionData;

csvFile = dir([rootDir filesep '*.csv']);

fileID = fopen(fullfile(rootDir,csvFile(5).name));
C = textscan(fileID,'%f64 %f64 %f64');
fclose(fileID);
c = cell2mat(C);
tmp = diff(c(:,1))';
ttl_up = find([0 tmp == 1]);
% get TTL length to estimate frame rate
% to get frameRate, assume behavior trial start TTL is up for 3sec
ttl_down = find([0 tmp == -1]);
frameRate = (ttl_down(1) - ttl_up(1))/3;

timestamps = pointGrey_ts_decoder(c(:,2));

meanFrame = [];
ind = 1;
vid = VideoReader(fullfile(rootDir,allFiles(4).name));
while hasFrame(vid)
    tmp = readFrame(vid);
    meanFrame(ind) = mean(mean(tmp(:,:,1)));
    ind = ind + 1;
end

ttl_down - ttl_up 
c(ttl_down,4) - c(ttl_up,4) 

diff(c(:,4))



%%
eventNames = {'outcome'};
nEventTypes = length(eventNames);
ev = [];
for tt = 1:min(behData.nTrials)
    trialEvents = behData.RawEvents.Trial{tt}.Events;
    % reward
    ev(tt) = trialStates.Outcome(1);    
end % end trial loop
cueA = behData.TrialTypes == 1;

%%
tmp = diff(c(:,2))';
trialStartSamp = find([0 tmp == 1]);

%%
plot(meanFrame); hold on; plot(c(:,2)+130)
plot(trialStartSamp(cueA),130*ones(length(trialStartSamp(cueA)),1),'r*')
%%

plot(diff(behData.TrialStartTimestamp));
hold on;
plot(framesPerTrial/40,'r--');













