% identify correct bonsai file based on
% behavior session
clearvars

%% user defined parameters
behFile = 'mar099_CuedReward_May04_2019_Session3';

%behRoot = 'Z:\users\fred\Data_Bpod';
behRoot = 'Z:\Data_Bpod';
drawRois = true;
loadRois = true;
nSegments = 2;

% --- extract mouse etc from file name ---
p = getBehFileInfo(behFile);

% --- load behavior file ---
load(fullfile(behRoot,p.mouse,p.protocol,'Session Data',behFile));
behData = SessionData;
trialStart_beh = behData.TrialStartTimestamp;
trialDur_beh = diff(trialStart_beh);

bonsaiPath = fullfile(behRoot,p.mouse,'Bonsai Videos');
[bestMatch,bon] = findBonsaiFile(behFile,p.date,trialDur_beh,bonsaiPath);
            
fprintf('found bonsai file with %d trials mismatch (bon-beh) \n',bestMatch.nTrialDiff);
fprintf('--- %s --- \n',bestMatch.csv);
fprintf('--- %s --- \n',bestMatch.avi);


%% extract video data
% --- use 'bon_extractFrameData' ---
vid = VideoReader(bestMatch.aviPath);
nFrames = length(bon.timeStamps);
frame_t0_tmp = readFrame(vid); % read the first frame
frame_t0 = double(frame_t0_tmp(:,:,1)); % take only one color channel (they are identical)

% --- manually draw rois using petr's RoiMaker gui ---
% returns struct 'rois' to workspace
% (double click into roi to commit it)
rois = 0;
if loadRois
    [fname,pathname] = uigetfile('align*.mat','select align*.mat with rois');
    tmp = load(fullfile(pathname,fname),'bon');
    rois = tmp.bon.rois;
elseif drawRois
    RoiMaker(frame_t0,[prctile(frame_t0(:),5) ...
                       prctile(frame_t0(:),95)]);
    while ~isstruct(rois) % to prevent the code from running on
        pause(0.2);
    end
    % if we accidentally made empty ROIs, get rid of them
    emptyIdx = arrayfun(@(r) nnz(r.footprint)==0, rois);
    rois = rois(~emptyIdx);
    
    for rr = 1:numel(rois)
        rois(rr).mask = reshape(rois(rr).footprint,...
                                vid.Height*vid.Width,1);
    end
end

% prepare image chunking
stepW = floor(vid.Width/nSegments);
vecW = 1:stepW:vid.Width;
if vecW(end) < vid.Width
    vecW = [vecW vid.Width];
end
stepH = floor(vid.Height/nSegments);
vecH = 1:stepH:vid.Height;
if vecH(end) < vid.Height
    vecH = [vecH vid.Height];
end

% initialise some variables
seg.mean = zeros(length(vecH)-1,length(vecW)-1,nFrames);
seg.diff = zeros(length(vecH)-1,length(vecW)-1,nFrames);
meanFrame = zeros(nFrames,1);
ind = 2;

% read one frame at a time until none are left
% first frame was read above, so this really starts at frame 2
disp(['reading video file...']);
while hasFrame(vid)
    frame_t1_tmp = readFrame(vid);
    frame_t1 = double(frame_t1_tmp(:,:,1));
    diffFrame = frame_t1 - frame_t0;
    
    if isstruct(rois)
        % extract rois from frame
        for rr = 1:numel(rois)
            rois(rr).diff(ind) = mean(abs((diffFrame(rois(rr).mask))));
            rois(rr).mean(ind) = mean(frame_t1(rois(rr).mask));
        end
    end

    % segment difference frame into chunk 'rois'
    for jj = 1:length(vecH)-1
        for kk = 1:length(vecW)-1
            tmp = diffFrame(vecH(jj):vecH(jj+1),vecW(kk):vecW(kk+1));
            seg.diff(jj,kk,ind) = mean(abs((tmp(:))));
            tmp = frame_t1(vecH(jj):vecH(jj+1),vecW(kk):vecW(kk+1));
            seg.mean(jj,kk,ind) = mean(tmp(:));
        end
    end
    
    % update variables
    ind = ind + 1;
    frame_t0 = frame_t1;
    if rem(ind,1000) == 0
        fprintf('--- frame %d of %d --- \n',ind,nFrames);
    end
end

% save image of rois drawn
figure;
if isstruct(rois)
    imRed = zeros(size(frame_t1,1),size(frame_t1,2),3);
    imRed(:,:,1) = 1;
    imshow(frame_t1,[]);
    hold on;
    hred = imshow(imRed);
    overlay = zeros(size(frame_t1));
    for rr = 1:length(rois)
        overlay(rois(rr).mask) = 0.25;
        tmp = find(rois(rr).mask,1);
        tmpX = rem(tmp,size(frame_t1,1));
        tmpY = floor(tmp/size(frame_t1,1));
        text(tmpY,tmpX,num2str(rr),'Color','w','FontSize',14);
    end
    hred.AlphaData = overlay;
    hold off;

    [tmp_path,tmp_fname] = fileparts(bestMatch.alignPath);
    saveas(gcf,fullfile(tmp_path,[tmp_fname '_rois.png']));
end

%% match behavior events to imaging frames
% for each imaging plane, find closest frame for each event
% this is done per behavior trial (behavior time starts at 0 each trial)
% the output is a logical array eventTypes x frames

% get the number of events and event names using 'getTrialEvents'
ev_tmp = getTrialEvents(behData,1);
eventNames = fieldnames(ev_tmp);
nEventTypes = length(eventNames);
% this is a logical array with true = behavioral event in that frame
frameEvents = false(nEventTypes,nFrames);

for tt = 1:behData.nTrials

    % get events of this trial
    ev = getTrialEvents(behData,tt);
    
    tmp_ind = bon.trialStart_samp(tt);
    thisOffset = bon.timeStamps(tmp_ind);
    
    for ee = 1:nEventTypes
        % timestamps of this event/trial
        thisEventTS = ev.(eventNames{ee});
        if ~isempty(thisEventTS)
            for ii = 1:length(thisEventTS)
                eventTime = thisEventTS(ii) + thisOffset;
                tmp = find(bon.timeStamps > eventTime,1);
                prevFrameDist = abs(eventTime-bon.timeStamps(tmp-1));
                nextFrameDist = abs(eventTime-bon.timeStamps(tmp));
                if prevFrameDist <= nextFrameDist
                    eventFrameInd = tmp-1;
                else
                    eventFrameInd = tmp;
                end
                frameEvents(ee,eventFrameInd) = true;
            end
        end
    end % end event loop
end % end trial loop
    
% make a vector nFrames long, each frame labeled with the behavioral trial
% it was collected in
trialNumVec = zeros(1,length(bon.timeStamps));
for tt = 1:min(behData.nTrials,bon.nTrials)
    thisTrialStart = bon.trialStart_samp(tt);
    trialNumVec(thisTrialStart:end) = tt;
end

% --- save stuff to .mat file ---
bon.trialNames = behData.TrialSettings.TrialsNames;
bon.trialTypes = behData.TrialTypes;
bon.trialNumVec = trialNumVec;
bon.events = frameEvents;
bon.eventNames = eventNames;
bon.rois = rois;
bon.seg.diff = seg.diff;
bon.seg.mean = seg.mean;
bon.img = frame_t0;

save(bestMatch.alignPath,'bon','bestMatch');


























































