% extract data from bonsai point grey video
% inputs:
%   vid - matlab VideoReader object
%   nFrames - number of frames in video, used for preallocation
%   nSegments - number of rows/columns for regular segmentation of image
%   drawRois - logical, =true if we want to draw rois
%   loadRois - logical, =true to load rois from other experiment (overrides drawRois)
%   varargin{1} - percentiles for frame display during roi drawing
% outputs:
%   seg.diff - frame to frame difference in each segment
%   seg.mean - mean of each frame in each segment
%   rois - struct containing rois drawn with diff, mean, footprint and mask
%   oneFrame - a single frame
%   figHandle - handle to rois figure

function [seg,rois,oneFrame,figHandle] = ...
                bon_extractFrameData(vid,nFrames,nSegments,drawRois,loadRois,varargin)
            
if nargin > 5
    percentiles = varargin{1};
else
    percentiles = [5 95];
end

frame_t0_tmp = readFrame(vid); % read the first frame
frame_t0 = double(frame_t0_tmp(:,:,1)); % take only one color channel (they are identical)
oneFrame = frame_t0;

% --- manually draw rois using petr's RoiMaker gui ---
% returns struct 'rois' to workspace
% (double click into roi to commit it)
rois = 0;
if loadRois
    [fname,pathname] = uigetfile('align*.mat','select align*.mat with rois');
    tmp = load(fullfile(pathname,fname),'bon');
    rois = tmp.bon.rois;
elseif drawRois
    RoiMaker(frame_t0,[prctile(frame_t0(:),percentiles(1)) ...
                       prctile(frame_t0(:),percentiles(2))]);
    while ~isstruct(rois) % to prevent the code from running on
        pause(0.2);
    end
    % if we accidentally made empty ROIs, get rid of them
    emptyIdx = arrayfun(@(r) nnz(r.footprint)==0, rois);
    rois = rois(~emptyIdx);
    
    for rr = 1:numel(rois)
        rois(rr).mask = reshape(rois(rr).footprint,...
                                vid.Height*vid.Width,1);
    end
end

% prepare image chunking
stepW = floor(vid.Width/nSegments);
vecW = 1:stepW:vid.Width;
if vecW(end) < vid.Width
    vecW = [vecW vid.Width];
end
stepH = floor(vid.Height/nSegments);
vecH = 1:stepH:vid.Height;
if vecH(end) < vid.Height
    vecH = [vecH vid.Height];
end

% initialise some variables
seg.mean = zeros(length(vecH)-1,length(vecW)-1,nFrames);
seg.diff = zeros(length(vecH)-1,length(vecW)-1,nFrames);
meanFrame = zeros(nFrames,1);
ind = 2;

% read one frame at a time until none are left
% first frame was read above, so this really starts at frame 2
disp(['reading video file...']);
while hasFrame(vid)
    frame_t1_tmp = readFrame(vid);
    frame_t1 = double(frame_t1_tmp(:,:,1));
    diffFrame = frame_t1 - frame_t0;
    
    if isstruct(rois)
        % extract rois from frame
        for rr = 1:numel(rois)
            rois(rr).diff(ind) = mean(diffFrame(rois(rr).mask));
            rois(rr).mean(ind) = mean(frame_t1(rois(rr).mask));
        end
    end

    % segment difference frame into chunk 'rois'
    for jj = 1:length(vecH)-1
        for kk = 1:length(vecW)-1
            tmp = diffFrame(vecH(jj):vecH(jj+1),vecW(kk):vecW(kk+1));
            seg.diff(jj,kk,ind) = mean(tmp(:));
            tmp = frame_t1(vecH(jj):vecH(jj+1),vecW(kk):vecW(kk+1));
            seg.mean(jj,kk,ind) = mean(tmp(:));
        end
    end
    
    % update variables
    ind = ind + 1;
    frame_t0 = frame_t1;
    if rem(ind,1000) == 0
        fprintf('--- frame %d of %d --- \n',ind,nFrames);
    end
end

% save image of rois drawn
if isstruct(rois)
    imRed = zeros(size(frame_t1,1),size(frame_t1,2),3);
    imRed(:,:,1) = 1;
    imshow(frame_t1,[]);
    hold on;
    hred = imshow(imRed);
    overlay = zeros(size(frame_t1));
    for rr = 1:length(rois)
        overlay(rois(rr).mask) = 0.25;
        tmp = find(rois(rr).mask,1);
        tmpX = rem(tmp,size(frame_t1,1));
        tmpY = floor(tmp/size(frame_t1,1));
        text(tmpY,tmpX,num2str(rr),'Color','w','FontSize',14);
    end
    hred.AlphaData = overlay;
    hold off;
    figHandle = gcf;
else
    figHandle = [];
end







