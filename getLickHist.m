% returns a lick histogram given edges for binning
% input:
%   licks - cell array of trials containing licks
%   edges - vector of bin edges in seconds relative time (just like the licks)
%           if not given, only out.trialVec and out.lickVec are returned
% output:
%   out.data - trials x bins (edges-1), lick counts
%   out.avg - mean trace / histogram
%   out.ci - 95% confidence interval from getCI.m
%   out.bootci - 95% confidence interval from bootci.m
%   out.trialVec - vector of trial numbers corresponding to licks
%   out.lickVec - vector of licks
%   out.xvec - bin middle points, same length as out.avg

function out = getLickHist(licks,varargin)

% optional input 'edges'
if nargin > 1
    edges = varargin{1};
else
    edges = 0;
end

% if 'licks' are in full raw behavior file format, first extract licks
% by default this is done relative to outcome state time of each trial
nTrials = length(licks);
if isstruct(licks{1})
    for ii = 1:nTrials
        outcomeTime = licks{ii}.States.Outcome(1);
        if isfield(licks{ii}.Events,'Port1In')
            licks_tmp{ii} = licks{ii}.Events.Port1In - outcomeTime;
        else
            licks_tmp{ii} = nan;
        end
    end
    licks = licks_tmp;
end


if edges == 0
    out.data = nan;
    out.avg = nan;
    out.ci = nan;
    out.bootci = nan;
    out.edges = nan;
else
    out.data = zeros(nTrials,length(edges)-1);

    for bb = 1:length(edges)-1
        thisStart = edges(bb);
        thisEnd = edges(bb+1);
        out.data(:,bb) = cellfun(@(x) sum(x > thisStart & x < thisEnd), licks);
    end

    binWidth = edges(2)-edges(1);
    out.avg = mean(out.data,1)/binWidth; % convert to licks/s
    out.ci = getCI(out.data/binWidth,0.05,1);
    if size(out.data,1) > 3
        out.bootci = bootci(1000,@mean,out.data/binWidth);
    else
        out.bootci = nan;
    end
    out.xvec = edges(1:end-1)+binWidth/2;
end

% make a cell array with trial numbers
% useful for lick raster plot
trials = {};
for jj = 1:length(licks)
    trials{jj} = jj*ones(1,length(licks{jj}));
end

out.trialVec = cell2mat(trials);
out.lickVec = cell2mat(licks);

        
        
        
        
        
        
        