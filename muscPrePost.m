%%% plot avg trace before / after inactivation across 2 sessions

clearvars; close all

%rootFolder = '/Volumes/winstor/swc/sjones/users/fred/Data_Bpod/';
rootFolder = 'Z:\Data_Bpod\';
protocol = 'CuedReward';

groupNumber = '11'; % refers to google sheet 'group' column

pre_time = -3;
post_time = 3;

antLickWindow = [-1.5 -0.02];
outLickWindow = [0 0.3];
antLickThreshold = 1;
outLickThreshold = 1;
lickBins = pre_time:0.2:post_time;
ylim = [-8 35];
xlim = [pre_time-0.1 post_time+0.1];
lineWidth = 2;

figureSize = [1 1 6 6];
pbasp = [1 1 1];
textSize = 14;
muscColor = [0.8 0.2 0.5];
regColor = [0.3 0.1 0.7];
stimColor = 0.9*[1 1 1];

colors = {regColor,muscColor};
%% ----------------

types = {'Cue A Reward' 'Uncued Reward'};

sheetName = 'muscimol';
gSheet = getGSheetString('Experiment Log',sheetName);
exps = GetGoogleSpreadsheet(gSheet.DOCID,gSheet.GID);

% fetch file names from google sheet data
tmp = strcmp(exps(1,:),'file name');
filenames = exps(2:end,tmp);
tmp = strcmp(exps(1,:),'group');
includeSession = strcmp(exps(2:end,tmp),groupNumber);
sessionNames = filenames(includeSession);
mouse = sessionNames{1}(1:6);

sessionFolder = fullfile(rootFolder,mouse,protocol,'Session Data','RewardA','Analysis');
savingFolder = fullfile(rootFolder,'aPlots',mouse);
if ~isdir(savingFolder)
    mkdir(savingFolder)
end

% concatenate sessions
[S,sessIndex] = concatSessions(sessionNames,sessionFolder);

% get parameters for plotting
stim_duration = S.Properties.GUISettings(1).SoundDuration;
stim_time = S.Data.CueTime(1,1,1);

% find samples pre and post to plot
timeVec = squeeze(S.Data.Photo_470.relativeTime(1,1,:));
sampInds = timeVec > pre_time & timeVec < post_time;
xvec = timeVec(sampInds);
xvec_licks = lickBins(1:end-1)+0.5*(lickBins(2)-lickBins(1));

% get logical indices for different trial types (types x trials)
trialInds = getTrialTypeIndices(S,types);

% classify trials based on licking
lickData = S.Data.Licks.Events;
antLick = sortLicking(antLickWindow,antLickThreshold,lickData);
outLick = sortLicking(outLickWindow,outLickThreshold,lickData);

%colors = colormap(lines(length(dNames)));
%colors = colormap(lines(5));

% plot

% loop over trial types
for tt = 1:length(types)

    for mm = 1:2 % loop over pre/post muscimol
        
        tmp = sessIndex == mm & trialInds(tt,:);
        if strcmp(types{tt},'Uncued Reward')
            thisTrials = tmp & outLick.hit.index;
        else
            thisTrials = tmp & antLick.hit.index & outLick.hit.index;
        end
        thisData = squeeze(S.Data.Photo_470.DFF(:,thisTrials,sampInds)); % trials x samples
        thisLicks = S.Data.Licks.Events(thisTrials);
        lickHist = getLickHist(thisLicks,lickBins);
        thisColor = colors{mm};

        %f0 = prctile(data(1:round(abs(sr * pre_time)),:),0.2,1);
        %data0 = bsxfun(@minus,thisData,f0);
        dataAvg = nanmean(thisData,1);

        % get confidence intervals for shaded error bar
        ci = bootci(1000,@mean,thisData);
        
        % --- plot stim shading ---
        if ~strcmp(types{tt},'Uncued Reward') && mm == 2
            subplot(2,length(types),tt); % data
            hold on;
            rh1 = rectangle('Position',[stim_time min(ci(:)) stim_duration ...
                                          abs(min(ci(:)))+max(ci(:))],...
                              'FaceColor',stimColor,'EdgeColor','none');
            subplot(2,length(types),tt+length(types)); % licks
            hold on;
            rh2 = rectangle('Position',[stim_time min(lickHist.avg) stim_duration ...
                                          abs(min(lickHist.ci(:)))+max(lickHist.avg)],...
                              'FaceColor',stimColor,'EdgeColor','none');
        end
        subplot(2,length(types),tt); % data
        hold on;
        shadedErrorBar(xvec,dataAvg,[ci(2,:)-dataAvg;dataAvg-ci(1,:)],...
                        {'LineWidth',lineWidth,'Color',thisColor},1);
        
        formatFigure(gcf,gca,textSize,0,10,...
                     'Time from reward [s]','DF/F [%]',xlim,ylim,...
                     1,pbasp,figureSize,0,1);
        title([types{tt}])
        
        subplot(2,length(types),tt+length(types)); % licks
        hold on;
%         shadedErrorBar(xvec_licks,lickHist.avg,[lickHist.ci(1,:);-lickHist.ci(2,:)],...
%                         {'LineWidth',lineWidth,'Color',thisColor},1);
        plot(xvec_licks,lickHist.avg,'LineWidth',lineWidth,'Color',thisColor);
        formatFigure(gcf,gca,textSize,0,10,...
                   'Time from reward [s]','Licks/s',xlim,0,...
                   1,pbasp,figureSize,0,1);

    end
    % figure grooming
    if ~strcmp(types{tt},'Uncued Reward')
        uistack(rh1,'bottom');
        uistack(rh2,'bottom');
    end

end

% get date from file name
[fileNameWithNewDate,dateOnly] = convertBpodDate(sessionNames{1},length([mouse protocol])+3);
fname = fullfile(savingFolder,['musc_' dateOnly{1} '.pdf']);
saveas(gcf,fname);
fname = fullfile(savingFolder,['musc_' dateOnly{1} '.eps']);
print(gcf,'-depsc','-painters',fname) 







