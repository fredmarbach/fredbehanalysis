% concatenate most fields in the Analysis structure
% this version works across mice
% inputs:
%   sessionNames - cell array of original session file names
%   rootFolder - full path to these files
% outputs:
%   S - analogous to Analysis structure, concatenated
%   sessIndex - 1 for session1 trials, 2 for session2 trials, etc
function [S,sessIndex] = concatSessionsMice(sessionNames,rootFolder)

nSessions = length(sessionNames);

% --- initialise all of the fields ---
% PROPERTIES
S.Properties.GUISettings = [];
S.Properties.RawEvents = [];
% DATA
% - trial types (numbers)
S.Data.TrialTypes = [];
% - states
S.Data.States = [];
S.Data.ZeroTime = [];
S.Data.CueTime = [];
S.Data.OutcomeTime = [];
% - licks
S.Data.Licks.Events = [];
% - photometry (alignment types x trials x samples)
S.Data.Photo_470.relativeTime = [];
S.Data.Photo_470.resampledData = [];
S.Data.Photo_470.DFF = [];
S.Data.Photo_470.Baseline = [];

S.Data.Photo_405.relativeTime = [];
S.Data.Photo_405.resampledData = [];
S.Data.Photo_405.DFF = [];
S.Data.Photo_405.Baseline = [];

sessIndex = [];

for ss = 1:nSessions
    thisSession = sessionNames{ss};
    % load new data
    tmp = strfind(thisSession,'_');
    mouse = thisSession(1:tmp(1)-1);
    protocol = thisSession(tmp(1)+1:tmp(2)-1);
    if strcmp(protocol,'CuedReward')
        load(fullfile(rootFolder,mouse,protocol,'Session Data','RewardA','Analysis',[thisSession '_Analysis.mat']));
    elseif strcmp(protocol,'BeliefState')
        load(fullfile(rootFolder,mouse,protocol,'Session Data','Analysis',[thisSession '_Analysis.mat'])); 
    else
        warning('add training phase in concatSessionsMice');
    end
    
    
    % PROPERTIES
    try
        S.Properties.GUISettings = [S.Properties.GUISettings Analysis.Properties.GUISettings];
    catch
        warning('GUISettings changes number of fields - not concatenated');
    end
    S.Properties.RawEvents = [S.Properties.RawEvents Analysis.Properties.RawEvents];
    % DATA
    % - trial types (numbers)
    S.Data.TrialTypes = [S.Data.TrialTypes Analysis.Data.TrialTypes];
    S.Data.nTrials(ss) = Analysis.Data.nTrials;
    % - states
    S.Data.States = [S.Data.States Analysis.Data.States];
    S.Data.ZeroTime = [S.Data.ZeroTime Analysis.Data.ZeroTime];
    S.Data.CueTime = cat(2,S.Data.CueTime,Analysis.Data.CueTime);
    S.Data.OutcomeTime = [S.Data.OutcomeTime Analysis.Data.OutcomeTime];
    % - licks
    S.Data.Licks.Events = [S.Data.Licks.Events Analysis.Data.Licks.Events];
    % - photometry (alignment types x trials x samples)
    S.Data.Photo_470.relativeTime = cat(2,S.Data.Photo_470.relativeTime,Analysis.Data.Photo_470.relativeTime);
    S.Data.Photo_470.resampledData = cat(2,S.Data.Photo_470.resampledData,Analysis.Data.Photo_470.resampledData);
    S.Data.Photo_470.DFF = cat(2,S.Data.Photo_470.DFF,Analysis.Data.Photo_470.DFF);
    S.Data.Photo_470.Baseline = cat(2,S.Data.Photo_470.Baseline,Analysis.Data.Photo_470.Baseline);
    
    S.Data.Photo_405.relativeTime = cat(2,S.Data.Photo_405.relativeTime,Analysis.Data.Photo_470.relativeTime);
    S.Data.Photo_405.resampledData = cat(2,S.Data.Photo_405.resampledData,Analysis.Data.Photo_470.resampledData);
    S.Data.Photo_405.DFF = cat(2,S.Data.Photo_405.DFF,Analysis.Data.Photo_470.DFF);
    S.Data.Photo_405.Baseline = cat(2,S.Data.Photo_405.Baseline,Analysis.Data.Photo_470.Baseline);
    
    sessIndex = [sessIndex ss*ones(1,Analysis.Data.nTrials)];
end

% cheating: some things are not concatenated, assumed to be the same
S.Properties.TrialNames = Analysis.Properties.TrialNames;










    