% extracts avg,med,max value from timeWindow
% inputs:
%       timeWindow - f.ex. [0 0.2] in seconds relative time
%   includedTrials - trials from S to consider; if = 1, all trials are used
%                S - standard data structure
%      varargin{1} - timeShift, a delay for each trial usually used for lick
%                    latency on each trial
% output:
%              out - fields avg, med, max

function out = getAvgValue(timeWindow,includedTrials,S,varargin)

% determine which trials
totalTrials = size(S.Data.Photo_470.DFF,2);
if length(includedTrials) == 1 % use all trials
    includedTrials = true(totalTrials,1);
else
    givenTrials = length(includedTrials);
    if givenTrials ~= totalTrials
        error('includedTrials logicals must match length of data structure')
    end
end
nIncludedTrials = sum(includedTrials);
includedTrials_inds = find(includedTrials);

if nargin > 3
    timeShift = varargin{1};
    % replace NaNs with 0 - these trials will usually be excluded anyways
    % later on, due to no licks (which has a lick latency of NaN)
    timeShift(isnan(timeShift)) = 0;
else
    timeShift = zeros(totalTrials,1);
end

out = struct;
for tt = 1:nIncludedTrials
    ind = includedTrials_inds(tt);
    thisTrace = squeeze(S.Data.Photo_470.DFF(:,ind,:)); % trials x samples
    timeVec = squeeze(S.Data.Photo_470.relativeTime(1,ind,:));
    sampInds = timeVec > timeWindow(1)+timeShift(ind) & ...
               timeVec < timeWindow(2)+timeShift(ind);
    out.avg(tt) = mean(thisTrace(sampInds));
    out.med(tt) = median(thisTrace(sampInds));
    out.max(tt) = max(thisTrace(sampInds));
end