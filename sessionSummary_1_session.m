% plot and save each trial type of each session


clearvars; close all;

antLickWindow = [-1.5 -0.02];
outLickWindow = [-0 0.5];
antLickThreshold = 1;
outLickThreshold = 1;

pre_time = -2;
post_time = 4;

markerSize = 1;
textSize = 14;
lineWidth = 2;
figureSize = [0 1 6 6];
stimColor = 0.9*[1 1 1];
avgColor = [0.4 0.2 0.8];
lickBins = pre_time:0.2:post_time;

%sessionName = 'mar073_CuedReward_Apr24_2018_Session1';
sessionName = 'mar075_CuedReward_Apr24_2019_Session3';
p = getBehFileInfo(sessionName);

%rootFolder = '/Volumes/winstor/swc/sjones/users/fred/Data_Bpod/';
rootFolder = 'Z:\Data_Bpod';
protocol = 'CuedReward';
%phase = 'RwdAB_PunC_val';
%phase = 'RewardA';
phase = 'RwdAB_PunC';


sessionFolder = fullfile(rootFolder,p.mouse,p.protocol,'Session Data',phase,'Analysis');



% make saving folder
savingFolder = fullfile(rootFolder,'aPlots',p.mouse,...
    'summary_individual',[p.date '_' sessionName]);
if ~isdir(savingFolder)
    mkdir(savingFolder)
end

% load data
load(fullfile(sessionFolder,[sessionName '_Analysis.mat']));
% get parameters for plotting
stim_duration = Analysis.Properties.GUISettings(1).SoundDuration;
stim_time = Analysis.Data.CueTime(1,1,1);

% find samples pre and post to plot
timeVec = squeeze(Analysis.Data.Photo_470.relativeTime(1,1,:));
sampInds = timeVec > pre_time & timeVec < post_time;
xvec = timeVec(sampInds);
stimSamp = find(xvec > stim_time,1,'first');
outSamp = find(xvec > 0,1,'first');

% get logical indices for different trial types (types x trials)
[trialInds,trialNames] = getTrialTypeIndices(Analysis);

% loop over number of data types (405 470 470b)
dataNames = fieldnames(Analysis.Data);
dataInds = find(contains(dataNames,'Photo'));
for dd = 1:length(dataInds)
    thisDataType = dataNames{dataInds(dd)};
    
    % --- plot: each trial type as image plot in a row ---
    nTrials = sum(trialInds,2);
    maxTrials = max(nTrials);
    nTypes = size(trialInds,1);
    for tt = 1:nTypes
        thisTrials = trialInds(tt,:);
        % --- image plot ---
        subplot(2,2,1)
        thisData = squeeze(Analysis.Data.(thisDataType).DFF(:,thisTrials,sampInds)); % trials x samples
        imagesc(thisData);
        hold on;
        line(stimSamp*[1 1],[0 maxTrials],'Color',0.5*[1 1 1],'LineWidth',1,'LineStyle','--');
        line(outSamp*[1 1],[0 maxTrials],'Color',0.5*[1 1 1],'LineWidth',1,'LineStyle','--');
        hold off;
        
        colormap(brewermap([],'*RdYlBu'))
        annotation('textbox', [0,0.95,0.7,0.05], 'string',...
            [trialNames{tt} ' (' strrep(thisDataType,'_',' ') ')'],...
            'Fontsize',textSize,'EdgeColor','none')
        xticklabels = pre_time:1:post_time;
        xticks = linspace(1,size(thisData,2),numel(xticklabels));
        set(gca, 'XTick', xticks, 'XTickLabel', xticklabels)
        formatFigure(gcf,gca,textSize,0,0,...
            'Time from Outcome [s]','Trials',0,[0 maxTrials],...
            1,[1 1 1],figureSize,0,1);
        
        
        % --- lick plot ---
        subplot(2,2,2);
        thisLicks = Analysis.Data.Licks.Events(thisTrials);
        out = getLickHist(thisLicks,lickBins);
        line([stim_time stim_time],[0 maxTrials],'Color',0.5*[1 1 1],'LineWidth',0.5);
        hold on;
        line([0 0],[0 maxTrials],'Color',0.5*[1 1 1],'LineWidth',0.5);
        plot(out.lickVec,out.trialVec,...
            'ks','MarkerFaceColor','k','MarkerSize',markerSize)
        hold off;
        
        formatFigure(gcf,gca,textSize,0,0,...
            'Time from Outcome [s]','Trials',[pre_time post_time],[0 maxTrials],...
            1,[1 1 1],figureSize,0,1);
        set(gca,'Ydir','reverse')
        set(gca,'XTick',xticklabels,'XTickLabels',xticklabels)
        
        
        % --- errorbar plot ---
        if sum(thisTrials) > 3
            % plot activity psth underneith
            subplot(2,2,3);
            dataAvg = nanmean(thisData,1);
            % get confidence intervals for shaded error bar
            ci = bootci(500,@mean,thisData);
            if ~contains(trialNames{tt},'Uncued') && contains(trialNames{tt},'Cue')
                rh = rectangle('Position',[stim_time min(ci(:)) stim_duration abs(min(ci(:)))+max(ci(:))],...
                    'FaceColor',stimColor,'EdgeColor','none');
            end
            hold on;
            line([0 0],[min(ci(1,:)) max(ci(2,:))],'Color',0.5*[1 1 1],'LineWidth',1,'LineStyle','--');
            shadedErrorBar(xvec,dataAvg,[ci(2,:)-dataAvg;dataAvg-ci(1,:)],...
                {'LineWidth',lineWidth,'color',avgColor},1);
            
            formatFigure(gcf,gca,textSize,0,0,...
                'Time from Outcome [s]','DF/F',[pre_time post_time],[min(ci(1,:)) max(ci(2,:))],...
                1,[1 1 1],figureSize,0,1);
            set(gca,'XTick',xticklabels,'XTickLabels',xticklabels)
        end
        hold off;
        
        % --- lick histogram plot ---
        if sum(thisTrials) > 3
            % plot activity psth underneith
            subplot(2,2,4);
            % get confidence intervals for shaded error bar
            if ~contains(trialNames{tt},'Uncued') && contains(trialNames{tt},'Cue')
                rh = rectangle('Position',[stim_time 0 stim_duration max(out.bootci(2,:))],...
                    'FaceColor',stimColor,'EdgeColor','none');
            end
            hold on;
            line([0 0],[min(out.bootci(1,:)) max(out.bootci(2,:))],'Color',0.5*[1 1 1],'LineWidth',1,'LineStyle','--');
            shadedErrorBar(out.xvec,out.avg,[out.bootci(2,:)-out.avg;out.avg-out.bootci(1,:)],...
                {'LineWidth',lineWidth,'color',avgColor},1);
            formatFigure(gcf,gca,textSize,0,0,...
                'Time from Outcome [s]','Licks/s',[pre_time post_time],[min(out.bootci(1,:)) max(out.bootci(2,:))],...
                1,[1 1 1],figureSize,0,1);
            set(gca,'XTick',xticklabels,'XTickLabels',xticklabels)
        end
        hold off;
        
        
        fname = fullfile(savingFolder,[thisDataType '_' trialNames{tt} '.png']);
        saveas(gcf,fname);
        fname = fullfile(savingFolder,[thisDataType '_' trialNames{tt} '.eps']);
        print(gcf,'-depsc','-painters',fname) 
        clf;
    end
    
end









