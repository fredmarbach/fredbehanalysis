% plot all trial types for each session of one mouse

clearvars; close all;

antLickWindow = [-1.5 -0.02];
outLickWindow = [-0 0.5];
antLickThreshold = 1;
outLickThreshold = 1;

pre_time = -3;
post_time = 3;

markerSize = 1;
textSize = 14;
lineWidth = 3;
figureSize = [0 1 17 6];
stimColor = 0.9*[1 1 1];
avgColor = [0.8 0.2 0.2];

mouse = 'FG036';
gSheet = getGSheetString('Experiment Log',mouse);
exps = GetGoogleSpreadsheet(gSheet.DOCID,gSheet.GID);

rootFolder = '/Volumes/winstor/swc/sjones/users/fred/Data_Bpod/';
protocol = 'CuedReward';
phase = 'RwdAB_PunC';

% fetch file names from google sheet data
tmp = strcmp(exps(1,:),'file name');
filenames = exps(2:end,tmp);
tmp = strcmp(exps(1,:),'date');
filedates = exps(2:end,tmp);
%tmp = strcmp(exps(1,:),'include');
%includeSession = strcmp(exps(2:end,tmp),'1');
sessionNames = filenames; %filenames(includeSession);
sessionFolder = fullfile(rootFolder,mouse,protocol,'Session Data',phase,'Analysis');
savingFolder = fullfile(rootFolder,'aPlots',mouse,'summary');
if ~isdir(savingFolder)
    mkdir(savingFolder)
end

        
for ss = 1:length(sessionNames)
    % load data
    load(fullfile(sessionFolder,[sessionNames{ss} '_Analysis.mat']));
    % get parameters for plotting
    stim_duration = Analysis.Properties.GUISettings(1).SoundDuration;
    stim_time = Analysis.Data.CueTime(1,1,1);

    % find samples pre and post to plot
    timeVec = squeeze(Analysis.Data.Photo_470.relativeTime(1,1,:));
    sampInds = timeVec > pre_time & timeVec < post_time;
    xvec = timeVec(sampInds);

    % get logical indices for different trial types (types x trials)
    [trialInds,trialNames] = getTrialTypeIndices(Analysis);

    % loop over number of data types (405 470 470b)
    dataNames = fieldnames(Analysis.Data);
    dataInds = find(contains(dataNames,'Photo'));
    for dd = 1:length(dataInds)
        thisDataType = dataNames{dataInds(dd)};
        
        % --- plot: each trial type as image plot in a row ---
        nTrials = sum(trialInds,2);
        maxTrials = max(nTrials);
        nTypes = size(trialInds,1);
        for tt = 1:nTypes
            thisTrials = trialInds(tt,:);
            % image plot
            subplot(2,nTypes*2,2*tt-1)
            thisData = squeeze(Analysis.Data.(thisDataType).DFF(:,thisTrials,sampInds)); % trials x samples
            imagesc(thisData);
            ylim([0 maxTrials])
            title(trialNames{tt});

            % lick plot
            subplot(2,nTypes*2,2*tt);
            thisLicks = Analysis.Data.Licks.Events(thisTrials);
            out = getLickHist(thisLicks);

            plot(out.lickVec,out.trialVec,...
                 'ks','MarkerFaceColor','k','MarkerSize',markerSize)
            set(gca,'Ydir','reverse')
            xlim([xvec(1) xvec(end)])
            ylim([0 maxTrials])

            if sum(thisTrials) > 3
                % plot activity psth underneith
                subplot(2,nTypes*2,nTypes*2+2*tt-1);
                dataAvg = nanmean(thisData,1);
                % get confidence intervals for shaded error bar
                ci = bootci(500,@mean,thisData);
                if ~strcmp(trialNames{tt},'Uncued Reward')
                    rh = rectangle('Position',[stim_time min(ci(:)) stim_duration abs(min(ci(:)))+max(ci(:))],...
                               'FaceColor',stimColor,'EdgeColor','none');
                end
                hold on;
                shadedErrorBar(xvec,dataAvg,[ci(2,:)-dataAvg;dataAvg-ci(1,:)],...
                                {'LineWidth',lineWidth,'color',avgColor},1);
                xlim([xvec(1) xvec(end)])
            end
        end
        formatFigure(gcf,gca,0,0,0,...
                       0,0,0,0,...
                       0,0,figureSize,0,1);
        fname = fullfile(savingFolder,[mouse '_' filedates{ss} '_' thisDataType '.png']);
        saveas(gcf,fname);
        clf;
    end
end








