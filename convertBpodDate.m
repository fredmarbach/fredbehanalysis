% convert Bpot string date to actual date that sorts properly
% input:
%   fileNames - usually the full file name in a cell array or single string
%   startOfMonth - index where date string starts
% output:
%   fileNameWithNewDate - converted filename
%   dateOnly - yymmdd cell array

function [fileNameWithNewDate,dateOnly] = convertBpodDate(fileNames,startOfMonth)

if ~iscell(fileNames)
    fileCell{1} = fileNames;
else
    fileCell = fileNames;
end

nFiles = length(fileCell);

fileNameWithNewDate = {};
dateOnly = {};
for ii = 1:nFiles
    thisString = fileCell{ii};
    thisMonth = thisString(startOfMonth:startOfMonth+2);
    thisDay = thisString(startOfMonth+3:startOfMonth+4);
    thisYear = thisString(startOfMonth+8:startOfMonth+9);

    switch thisMonth
        case 'Jan'
            monthNum = '01';
        case 'Feb'
            monthNum = '02';
        case 'Mar'
            monthNum = '03';
        case 'Apr'
            monthNum = '04';
        case 'May'
            monthNum = '05';
        case 'Jun'
            monthNum = '06';
        case 'Jul'
            monthNum = '07';
        case 'Aug'
            monthNum = '08';
        case 'Sep'
            monthNum = '09';
        case 'Oct'
            monthNum = '10';
        case 'Nov'
            monthNum = '11';
        case 'Dec'
            monthNum = '12';
    end

    fileNameWithNewDate{ii} = [thisString(1:startOfMonth-1) ...
                thisYear monthNum thisDay ...
                thisString(startOfMonth+10:end)];
    dateOnly{ii} = [thisYear monthNum thisDay];
end


